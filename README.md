# README #

Version: 0.0.2-alpha

This game is an online clone of the game 
[Stupiduel by Lost Adept Distractions](http://lostadept.com/), with a few 
twists/additions. Do understand this software is very much still in development 
and contains many bugs, many of which haven't been discovered yet.

### Requirements to run this project on a server: ###

* PHP 5.6+ (Tested with 7.0.13)
* MySQL 5.6+ (Tested with 5.6.27)
* MySQLi 5+ (Tested with 5.0.12)

### How do I get set up? ###

Clone the repo into the appropriate directory for your web server. Open the 
project in your browser and follow the initial setup.

### How do I report bugs/suggestions? ###

Please use the built-in 
[issue tracking in BitBucket](https://bitbucket.org/Sweylo/stupid-duel/issues)

### How is this licensed? ###

This project is licensed under the ISC license, a copy of which can be found in 
the docs directory in the source.
