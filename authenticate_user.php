<?php

$user_check = false;
include('global.php');

$username = $_POST['username'];
$password = $_POST['password'];

$user = $db -> select_one_from('users', 'user_name', $username);

if (!$user) {
	die("User '$username' not found.");
}

$sql = "SELECT COUNT(user_id) AS num_of_users FROM users";
$result = mysqli_query($db, $sql);

if ($row = mysqli_fetch_assoc($result)) {
	$num_of_users = $row['num_of_users'];
} else {
	echo 'error finding user';
}

if (sha1($username . $password) == $user['user_password']) {

    setcookie(
        'user',
        $username,
        time() + (36000)
    );

	do {
		$login_session = sha1(mt_rand(-$num_of_users, $num_of_users) + $num_of_users);
	} while ($login_session == $user['user_login_session']);

	setcookie(
        'login_session',
        $login_session,
        time() + (36000)
    );

	$sql = "UPDATE users SET user_login_session = '$login_session' WHERE user_id = '$user[user_id]'";

	if ($db -> query($sql) == true) {
	    header('Location: index.php');
	} else {
		die('Error updating user');
	}

} else {
	echo "Incorrect password for user '$username'.";
}

?>
