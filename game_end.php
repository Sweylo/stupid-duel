<?php

$user_check = true;
include('global.php');

$game = $db -> select_one_from('games', 'game_id', $_GET['game_id']);

$players = $db -> select(
	"SELECT * "
	. "FROM users u JOIN game_users gu "
		. "ON u.user_id = gu.user_id "
	. "WHERE gu.game_id = $game[game_id] "
	. "ORDER BY gu.game_user_order"
);

foreach ($players as $player) {
	if ($player['game_user_lives'] > 0) {
		$winner = $player;
	}
}

?>

<div class="content">

	<div class="content-header">
		<h1>Game over</h1>
		<div class="clear"></div>
	</div>
	
	<p><?php echo $winner['user_name']; ?> wins!</p>
	<?php if ($user['user_id'] == $game['owner_user_id']) { ?>
	<p>
		As the owner of the game, you can either
		<a href="game_reset.php?game_id=<?php echo $game['game_id']; ?>">
			reset</a>
		or
		<a href="game_delete.php?game_id=<?php echo $game['game_id']; ?>">
			delete</a>
		the game.
	</p>
	<?php } ?>

</div>

<?php include('footer.php'); ?>