<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set("display_errors", 1);
date_default_timezone_set('UTC');

if ($_GET['mode'] == 'submit') {
	
	// if the port wasn't specified, set it to 3306 (mysqld default)
	if (!$_POST['mysql_port']) {
		$_POST['mysql_port'] = 3306;
	}
	
	// translate checkbox value to boolean
	$_POST['enable_guest'] = $_POST['enable_guest'] == 'on';

	// need to do a preliminary connection check

	// encode the data to json and write it out to the file
	$config_file = fopen('config.json', 'w');
	fwrite($config_file, json_encode($_POST, JSON_PRETTY_PRINT));
	fclose($config_file);

	header('Location: index.php');

}

?>

<!DOCTYPE html>

<head>
	<title>Stupid-Duel - Initial Setup</title>
	<link rel="stylesheet" type="text/css" href="css/reset.css" />
	<link rel="stylesheet" type="text/css" href="css/fonts.css" />
	<link rel="stylesheet" type="text/css" href="css/main.css" />
	<script src="js/jquery-3.1.1.min.js"></script>
</head>

<style>
	body {
		height: 340px;
		width: 600px;
		position: absolute;
		top: 50%;
		left: 50%;
		margin-top: -170px;
		margin-left: -300px;
	}
	.content input[type=number] {
		margin-bottom: 20px;
	}
	.content input[name=mysql_host] {
		width: 350px;
		float: left;
		clear: left;
	}
	.content input[name=mysql_port] {
		width: 210px;
		float: right;
		clear: right;
	}
	.content input[type=text][name=mysql_user] {
		margin-bottom: -1px;
	}
</style>

<body>
	
<div class="content">

<form method="post" action="create_config.php?mode=submit">

    <div class="content-header">
		<h1>Initial Setup</h1>
		<input class="button start" type="submit" value="Save">
		<div class="clear"></div>
	</div>

    <div>
		<input type="text" name="mysql_host" placeholder="MySQL hostname">
	</div>
	<div>
		<input type="number" name="mysql_port"
			placeholder="MySQL port (default: 3306)">
	</div>
	<div>
		<input type="text" name="mysql_user" placeholder="MySQL username">
	</div>
	<div>
		<input type="text" name="mysql_password" placeholder="MySQL password">
	</div>
	<div>
		<input type="text" name="mysql_db" placeholder="MySQL DB name">
	</div>
	<div>
		<input type="text" name="security_key" 
			placeholder="Security key (required to create a user)">
	</div>
	<div>
		<label>
			<input type="checkbox" name="enable_guest" checked>
			<span>Enable the guest account</span>
		</label>
	</div>

</form>
	
</div>

</body>
