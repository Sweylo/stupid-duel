<?php

$user_check = true;
include('global.php');

$game = $db -> select_one_from('games', 'game_id', $_GET['game_id']);
$game_cards =
	$db -> select_many_from('game_cards', 'game_id', $_GET['game_id']);

$sql = "SELECT *
		FROM users u JOIN game_users gu
			ON u.user_id = gu.user_id
		WHERE gu.game_id = $game[game_id]
		ORDER BY u.user_name";
$result = mysqli_query($db, $sql);
$players = array();

while ($row = mysqli_fetch_assoc($result)) {
	array_push($players, $row);
}

if (count($players) < 3) {
	echo "<a href='game_info.php?game_id=$game[game_id]'>
		&laquo; Back</a><br />";
	echo "Not enough players to start the game. Player count: "
		. count($players);
	die();
}

if ($game['owner_user_id'] != $user['user_id']) {
	die("Only the owner of the game can start it.");
}

$sql = "UPDATE games SET game_turn = 1, game_start_time = NOW() "
	.	"WHERE game_id = $game[game_id]";

if ($db -> query($sql) == false) {
	die('Unable start game: ' . $db -> error);
}

// deal out cards to players
$num_of_cards_in_hand = 0;
$card_order_index = 0;

while ($num_of_cards_in_hand < 5 && $card_order_index < count($game_cards)) {
	foreach ($players as $player) {
		$sql = "UPDATE game_cards
				SET user_hand_id = $player[user_id]
				WHERE card_order_index = $card_order_index";
		echo "$sql<br />";
		if ($db -> query($sql) == false) {
			die('Unable deal card: ' . $db -> error);
		}
		$card_order_index++;
	}
	$num_of_cards_in_hand++;
}

header("Location: play.php?game_id=$game[game_id]");

?>
