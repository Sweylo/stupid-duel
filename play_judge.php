<?php

$user_check = true;
include('global.php');

print_r($_POST);

// find if this judge is the last one to vote, and if it is, run end-of-round
// procedures to move the game on to the next round
$voters = $db -> select(
	"SELECT * "
	. "FROM game_users "
	. "WHERE game_id = $_POST[game_id] "
		. "AND game_user_vote = 0 "
		. "AND user_id <> $_POST[attack_player_id] "
		. "AND user_id <> $_POST[defend_player_id] "
);

// update the database with the player's vote
$db -> alter(
	"UPDATE game_users "
	. "SET game_user_vote = $_POST[vote] "
	. "WHERE user_id = $_POST[this_player_id] "
		. "AND game_id = $_POST[game_id]"
) or die('Unable to update user: ' . $db -> error);

if (count($voters) == 1) {
	echo 'last voter';
	header("Location: play_turn_end.php?game_id=$_POST[game_id]");
} else {
	echo 'not last voter';
	header("Location: play.php?game_id=$_POST[game_id]");
}

?>
