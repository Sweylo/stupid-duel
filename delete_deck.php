<?php

$user_check = true;
include('global.php');

$types = array('item', 'mod', 'god', 'wild');
$deck = $db -> select_one_from('decks', 'deck_id', $_GET['id']);
$cards = $db -> select_many_from('cards', 'card_deck_id', $deck['deck_id']);

$sql =
	"SELECT * FROM decks d JOIN games g ON d.deck_id = g.deck_id WHERE d.deck_id = $deck[deck_id]";
$result = mysqli_query($db, $sql);

if (mysqli_num_rows($result) > 0) {
	$row = mysqli_fetch_assoc($result);
	die("Unable to delete: this deck is currently being used in the game '$row[game_name]'.");
}

if ($deck['deck_creator_user_id'] == $user['user_id'] || $user['user_permission_level'] > 1) {

	foreach ($cards as $card) {

		foreach ($types as $type) {

			$sql = "DELETE FROM card_${type}s WHERE ${type}_card_id = $card[card_id]";

			if ($db -> query($sql) == true) {
			    //header("Location: game_info.php?id=$_GET[id]");
			} else {
				echo "<b>Error deleting deck:</b> <em>" . $db -> error . "</em>";
			}

		}

		$sql = "DELETE FROM cards WHERE card_id = $card[card_id]";

		if ($db -> query($sql) == true) {
			//header("Location: game_info.php?id=$_GET[id]");
		} else {
			echo "<b>Error deleting deck:</b> <em>" . $db -> error . "</em>";
		}

	}

	$sql = "SELECT max(card_id) as card_id_max FROM cards";
    $result = mysqli_query($db, $sql);

    if ($row = mysqli_fetch_assoc($result)) {
        $card_id_max = ($row["card_id_max"]) ? $row["card_id_max"] : 0;
    } else {
        die($db -> error);
    }

	$sql = "ALTER TABLE cards AUTO_INCREMENT = $card_id_max";
	if ($db -> query($sql) == false) {
	    die($db -> error);
	}

	$sql = "DELETE FROM decks WHERE deck_id = $deck[deck_id]";

	if ($db -> query($sql) == true) {
		header("Location: index.php");
	} else {
		die($db -> error);
	}

} else {
	die("Decks can only be deleted by their creators or admins, bitch.");
}

?>
