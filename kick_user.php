<?php

$user_check = true;
include('global.php');

if ($user_perm > USER_PLEB || $user['user_id'] == $_GET['user_id']) {

	$sql = "SELECT *
			FROM users u JOIN game_users gu
				ON u.user_id = gu.user_id
			WHERE gu.game_id = $_GET[game_id]";
	$result = mysqli_query($db, $sql);
	if (mysqli_num_rows($result) == 3) {
		header("Location: play.php?id=$_GET[game_id]&error=too_few_players");
		die();
	}

	$sql = "DELETE FROM game_users
			WHERE game_id = $_GET[game_id] AND user_id = $_GET[user_id]";

	echo "$sql<br />";

	if ($db -> query($sql) == false) {
		die('Unable to update card: ' . $db -> error);
	}

	header("Location: play.php?id=$_GET[game_id]");

}

?>
