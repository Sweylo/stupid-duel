<?php

$user_check = true;
include('global.php');

if ($user_perm != USER_GUEST) {
	die('You need to be logged in to join a game.');
}

$game = $db -> select_one_from('games', 'game_id', $_GET['game_id']);

if (!$game) {
	echo "The game cannot be found. It must have been deleted.";
}

if ($game['game_password'] && $_POST['password'] != $game['game_password']) {
	echo "<a href='lobby.php?id=$game[game_id]'>&laquo; Back</a><br />";
	die("Incorrect game password.");
}

// get next available user order index

$sql = "SELECT MAX(game_user_order) + 1 AS next_user_order
		FROM game_users
		WHERE game_id = $_GET[game_id]";
$result = mysqli_query($db, $sql);
$row = mysqli_fetch_assoc($result);

// associate the user with the game

$sql = "INSERT INTO game_users (game_id, user_id, game_user_order)
		VALUES ($_GET[game_id], $user[user_id], $row[next_user_order])";

if ($db -> query($sql) == true) {
    header("Location: lobby.php?game_id=$_GET[game_id]");
}

echo "<b>Error joining game:</b> <em>" . $db -> error . "</em>";

?>
