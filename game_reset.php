<?php

$user_check = true;
include('global.php');

$game = $db -> select_one_from('games', 'game_id', $_GET['game_id']);

if ($user['user_id'] == $game['owner_user_id']) {

    // update game values back to default

    $sql = "UPDATE games
            SET game_over = 0, game_turn = 0, game_start_time = NULL
            WHERE game_id = $game[game_id]";

    //echo "$sql<br />";

    if ($db -> query($sql) == false) {
        die('Unable to update game: ' . $db -> error);
    }

    // delete all users associated with the game, except the owner

    $sql = "DELETE FROM game_users
            WHERE game_id = $game[game_id]
                AND user_id <> $game[owner_user_id]";

    //echo "$sql<br />";

    if ($db -> query($sql) == false) {
        die('Unable to update user: ' . $db -> error);
    }
	
	// reset owner lives
	
	$sql = "UPDATE game_users "
            . "SET game_user_lives = 0 "
            . "WHERE game_id = $game[game_id] "
				. "AND user_id = $game[owner_user_id]";

    //echo "$sql<br />";

    if ($db -> query($sql) == false) {
        die('Unable to update game: ' . $db -> error);
    }

    // shuffle and place all cards back in the deck

    $sql = "SELECT *
    		FROM game_cards
            WHERE game_id = $game[game_id]";
    $result = mysqli_query($db, $sql);
    $cards = array();

    while ($row = mysqli_fetch_assoc($result)) {
    	array_push($cards, $row);
        //print_r($row);
    }

    shuffle($cards);

    foreach ($cards as $key => $card) {

        $sql = "UPDATE game_cards
                SET user_hand_id = NULL,
                    game_card_field_status = 0,
                    card_order_index = $key
                WHERE game_id = $game[game_id]
                    AND game_card_id = $card[game_card_id]";

        //echo "$sql<br />";

        if ($db -> query($sql) == false) {
            die('Unable to update game: ' . $db -> error);
        }

    }

    header("Location: lobby.php?game_id=$game[game_id]");

} else {
    echo "You don't have permission to reset this game";
}

?>
