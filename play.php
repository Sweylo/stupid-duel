<?php

$user_check = true;
include_once('global.php');

$gid = $_GET['game_id'];
$game = $db -> select_one_from('games', 'game_id', $gid);
$game_cards = $db -> select_many_from('game_cards', 'game_id', $gid);

// kill the page if the game object is null
if (!$game) { 
	//die("Game (id#$gid) doesn't exist."); 
	header("Location: index.php?error=gamenoexist&game_id=$gid");
}

// determine if there's a winner and redirect if so
$win = $db -> select (
	"SELECT * "
	. "FROM users u JOIN game_users gu "
		. "ON u.user_id = gu.user_id "
	. "WHERE gu.game_id = $game[game_id] "
		. "AND game_user_lives > 0 "
	. "ORDER BY gu.game_user_order"
);
if (count($win) == 1) {

	// update the game to indicate the game's over
	$db -> alter(
		"UPDATE games "
		. "SET game_over = 1 "
		. "WHERE game_id = $game[game_id]"
	) or die('1Unable to update user: ' . $db -> error);

	header("Location: game_end.php?game_id=$game[game_id]");
	die();
	
}

// determine the current stage of the turn: attack, defend, or judge
$is_attack = $game['game_attack_scenario'] == ''
	&& $game['game_defend_scenario'] == '';
$is_defend = $game['game_attack_scenario'] != ''
	&& $game['game_defend_scenario'] == '';
$is_judge = $game['game_attack_scenario'] != ''
	&& $game['game_defend_scenario'] != '';

echo "<!--is_attack: " . ($is_attack ? 'true' : 'false') . "-->";
echo "<!--is_defend: " . ($is_defend ? 'true' : 'false') . "-->";
echo "<!--is_judge: " . ($is_judge ? 'true' : 'false') . "-->";

// get an array containing all players that are in the game
$players = $db -> select(
	"SELECT * "
	. "FROM users u JOIN game_users gu "
	. "ON u.user_id = gu.user_id "
	. "WHERE gu.game_id = $gid"
);

if (count($players) < 3) {
	die("Number of players too low, need at least 3.");
}

// determine which player's turn it is
$player_turn = ($game['game_turn'] % count($players) == 0) ?
	 count($players) : $game['game_turn'] % count($players);

// assign players to their respective roles
$judges = array();
foreach ($players as $key => $player) {
	if ($player['user_id'] == $user['user_id']) {
		$this_player = $player;
	}
	if ($player['game_user_order'] == $player_turn) {
		$attack_player = $player;
	}
	if ($player['user_id'] == $game['game_defender_id']) {
		$defend_player = $player;
	}
	if ($is_judge && $player != $attack_player && $player != $defend_player) {
		array_push($judges, $player);
	}
	// assign the boolean extra field 'is_targetable' to each player
	$players[$key]['is_targetable'] = false;
	if (
		$is_attack 
		&& 
		$this_player == $attack_player
		&&
		$player['user_id'] != $attack_player['user_id']
		&&
		$player['game_user_lives'] > 0
	) {
		$players[$key]['is_targetable'] = true;
	}
}

echo "<!--this_player: $this_player[user_name]-->";
echo "<!--attack_player: $attack_player[user_name]-->";
echo "<!--defend_player: $defend_player[user_name]-->";

// get the votes from the voting players if in the judging stage
if ($is_judge) {

	$result = $db -> select(
		"SELECT game_user_vote "
		. "FROM game_users "
		. "WHERE game_id = $gid "
		. "AND user_id <> $attack_player[user_id] "
		. "AND user_id <> $defend_player[user_id] "
	);
	$vote = 0;
	$vote_count = 0;
	
	foreach ($result as $row) {
		$vote += $row['game_user_vote'];
		if ($row['game_user_vote'] != 0) {
			$vote_count++;
		}
	}

}

// determine if there are players that still haven't voted
$still_voting = count($players) - 2 > $vote_count;

// get this player's cards in an array
if ($this_player) {

	$this_player_cards = $db -> select(
		"SELECT * "
		. "FROM users u JOIN game_cards gc "
		. "ON u.user_id = gc.user_hand_id "
		. "WHERE gc.game_id = $gid "
		. "AND u.user_id = $this_player[user_id]"
	);
	
	foreach ($this_player_cards as $key => $card) {
		$this_player_cards[$key] = $db -> get_card($card['card_id']);
	}

}

// assign name of the action button
if ($is_attack) { $action_name = 'Attack'; }
if ($is_defend) { $action_name = 'Defend'; }
if ($is_judge) { $action_name = 'Judge'; }

// assign boolean shortcuts
$this_is_attack_player = $is_attack && $this_player == $attack_player;
$this_is_not_attack_player = $is_attack && $this_player != $attack_player;
$this_is_defend_player = $is_defend && $this_player == $defend_player;
$this_is_not_defend_player = $is_defend && $this_player != $defend_player;
$this_is_judge = in_array($this_player, $judges);
$this_player_has_voted = $this_player['game_user_vote'] != 0;
$good_to_refresh = 
	!$this_is_attack_player 
	&& 
	!$this_is_defend_player 
	&& 
	!($this_is_judge && !$this_player_has_voted)
	&&
	!(
		$game['game_coin_toss'] 
		&& 
		$this_player['user_id'] == $defend_player['user_id']
	);

if ($this_is_attack_player) {
	
	// advance to next turn if the attacking player has already been defeated

	if ($attack_player['game_user_lives'] == 0) {
		$new_turn = $game['game_turn'] + 1;
		$db -> alter(
			"UPDATE games "
			. "SET game_turn = $new_turn "
			. "WHERE game_id = $game[game_id]"
		) or die('Unable to update user: ' . $db -> error);
	}
	
	$play_status = 'Attack Phase';
	
} else if ($this_is_not_attack_player) {
	$play_status = "Attack Phase - Waiting on $attack_player[user_name]";
} else if ($this_is_defend_player) {
	$play_status = 'Defend Phase';
} else if ($this_is_not_defend_player) {
	$play_status = "Defend Phase - Waiting on $defend_player[user_name]";
} else if ($is_judge && !$game['game_coin_toss']) {
	$play_status = "Judge Phase - Waiting on votes";
} else if ($is_judge && $game['game_coin_toss']) {
	$play_status = "Judge Phase - Waiting on coin toss";
}

?>

<link rel="stylesheet" type="text/css" href="css/gameplay.css" />
<script> var game_id = <?php echo $game['game_id']; ?>; </script>
<script src="js/gameplay.js"></script>

<div class="content-header">
	<h1><?php echo $game['game_name']; ?></h1>
	<span id="error">
	<?php
	switch ($_GET['error']) {
		case true:
			echo ' - Error: ';
		case 'no_target':
			echo 'no target selected';
			break;
		case 'no_main_card':
			echo 'no main card selected';
			break;
		case 'no_scenario':
			echo 'no scenario described';
			break;
		case 'too_few_players':
			echo 'too few players to kick anyone';
			break;
	}
	?>
	</span>
	<div id="notifications">
		<div id="notifications-refresh" class="refresh">
			
		</div>
	</div>
	<div id="game-action">
		<?php if ($user['user_id'] == $game['owner_user_id']) { ?>
		<a class="button delete" 
		   href="game_delete.php?game_id=<?php echo $game['game_id']; ?>">
			Delete
		</a>
		<?php } ?>
	</div>
	<div class="clear"></div>
</div>

<div id="field" class="content">

	<?php if ($is_attack) { ?>
	<form method="post" action="play_attack.php">
	<?php } else if ($is_defend) { ?>
	<form method="post" action="play_defend.php">
	<?php } else if ($is_judge && !$game['game_coin_toss']) { ?>
	<form method="post" action="play_judge.php">
	<?php } else if ($is_judge && $game['game_coin_toss']) { ?>
	<form method="post" 
		action="play_turn_end.php?game_id=<?php echo $game['game_id']; ?>">
	<?php } ?>

	<input class="hidden" type="text" name="game_id"
		value="<?php echo $gid; ?>">
	<input class="hidden" type="text" name="attack_player_id"
		value="<?php echo $attack_player['user_id']; ?>">
	<input class="hidden" type="text" name="defend_player_id"
		value="<?php echo $defend_player['user_id']; ?>">
	<input class="hidden" type="text" name="this_player_id"
		value="<?php echo $this_player['user_id']; ?>">

	<div id="player-list">
		<?php if ($good_to_refresh) { ?>
		<div id="player-list-refresh" class="refresh">
		<?php } ?>
		<table id="players">
			<thead>
				<tr>
					<?php
					if ($this_is_attack_player) { ?>
					<th>&nbsp;</th>
					<?php } ?>
					<th width="10px"></th>
					<th>Player</th>
					<?php if ($is_judge) { ?>
					<th width="1px">Vote</th>
					<?php } ?>
					<th width="1px">Hits</th>
				</tr>
			</thead>
			<?php
			echo "<!--$defend_player[user_name]-->";
			foreach ($players as $player) {
				
				echo "<!--$player[user_name]-->";
				
				// assign different classes to indicate different states
				if ($player['game_user_lives'] == 0) {
					echo '<tr class="out">'; 
				} else if ($player['user_id'] == $defend_player['user_id']) {
					//echo '<tr class="selected">';
				} else if ($this_is_attack_player && $player['is_targetable']) {
					echo '<tr class="targetable">';
				} else {
					echo '<tr>';
				}
				
				// add radio buttons for attack selection if is the attacker
				if ($this_is_attack_player) {
					if ($player['is_targetable']) {
						echo 
							"<td class='target' width='1px'>
								<input type='radio' name='target_user_id'
									value='$player[user_id]'></td>";
					} else {
						echo "<td class='target' width='1px'>&nbsp;</td>";
					}
				}
				
				// echo appropriate icon for attacker/defender
				if ($player['user_id'] == $attack_player['user_id']) {
					echo "<td class='icon'><span>&#9876;</span></td>";
				} else if ($player['user_id'] == $defend_player['user_id']) {
					echo "<td class='icon'><span>&#9960;</span></td>";
				} else if ($player['game_user_lives'] == 0) { 
					echo "<td class='icon'><span>&#9760;</span></td>";
				} else {
					echo "<td class='icon'></td>";
				}
				
				// output player name
				echo "<td class='player'>$player[user_name]";
				
				// if player is a mod/admin, add the kick option
				if ($user_perm > USER_PLEB) {
					echo " - <a href='kick_user.php?game_id="
					 	. "$gid&user_id=$player[user_id]'>kick</a>";
				}
				echo "</td>";
				
				// show the user's vote if the game is the judge phase
				if ($is_judge) {
					echo "<td class='vote'>";
					if ($player['game_user_vote'] != 0) {
						echo ($player['game_user_vote'] > 0)
							? "&check;" : "&cross;";
					} else {
						echo '-';
					}
					echo '</td>';
				}
				
				// output the number of hits
				echo '<td class="hits">'
					. (3 - $player['game_user_lives'])
					. '</td>';
				echo '</tr>';
			}
			?>
		</table>
		
		<?php if ($is_judge && !$game['game_coin_toss']) { ?>
		<div id="vote">
			<h3>Vote</h3>
				<?php
				if (
					$this_player
					&&
					$this_player['user_id'] != $attack_player['user_id']
					&&
					$this_player['user_id'] != $defend_player['user_id']
					&&
					$still_voting
					&&
					!$this_player_has_voted
				) {
				?>
				<p>What happens to
					<b><?php echo $defend_player['user_name']; ?></b>?</p>
				<div id="dies">
					<label>
						<input type="radio" name="vote" value="-1">Dies
					</label>
				</div>
				<div id="lives">
					<label>
						<input type="radio" name="vote" value="1">Lives
					</label>
				</div>
				<input class="button" type="submit" value="Vote">
				<?php
				} else {
					if ($still_voting) {
				?>
				<p>Waiting on votes from:</p>
				<ul id="waiting">
				<?php
				foreach ($judges as $judge) {
					if ($judge['game_user_vote'] == 0) {
						echo "<li>$judge[user_name]</li>";
					}
				}
				?>
				</ul>
			<?php }} ?>
		</div>
		<?php } else if ($is_judge && $game['game_coin_toss']) { ?>
			<h2 class="side">Coin Toss</h2>
			<?php if ($this_player['user_id'] == $defend_player['user_id']) { ?>
			<h3>Call the toss:</h3>
			<ul>
				<li><input type="radio" name="call" value="heads"> Heads</li>
				<li><input type="radio" name="call" value="tails"> Tails</li>
			</ul><br />
			<input class="button" type="submit" value="Flip the coin">
			<?php } else { ?>
			<h3>
				Waiting on 
				<?php echo $defend_player['user_name']; ?> 
				to call the toss
			</h3>
			<?php } ?>
		<?php } ?>
			
		<?php if ($good_to_refresh) { ?>
		</div>
		<?php } ?>
		
	</div>

	<div id="play-area">
		
	<?php if ($good_to_refresh) { ?>
	<div id="play-area-refresh" class="refresh">
	<?php } ?>

		<div id="play-area-status">
			<h2><?php echo $play_status; ?></h2>
		</div>
		
		<?php if ($this_player && $this_player['game_user_lives'] == 0) { ?>
		<div id="defeat">
			<h2>You Lost</h2>
			<p>You've run out of lives, but you can still stay and vote.</p>
			<br />
			<?php
			if (count($players) > 3) {
				echo "<p><a href='kick_user.php
					?user_id=$this_player[user_id]&game_id=$gid'>";
			?>
				Or you can leave like a little bitch (no more voting).
			</a></p>
			<?php } ?>
		</div>
		<?php } else if (!$this_player) { ?>
		<h4>
			You're spectating this game. You can watch, but not play or vote.
		</h4>
		<?php } ?>

		<div id="scenario">
			<?php if ($is_defend || $is_judge) { ?>
			<div id="attack">
				<p id="actors">
				<?php 
					echo $attack_player['user_name']
						. "&rarr;"
						. $defend_player['user_name'];
				?>
				</p>
				<div class="seperator"></div>
				<p class="description">
					<b>Attack: </b>
					<?php echo $game['game_attack_scenario']; ?>
				</p>
				<ul class="cards small">
					<?php
					foreach ($game_cards as $game_card) {
						if ($game_card['game_card_field_status'] == 1) {
							$card = $db -> get_card($game_card['card_id']);
					?>
					<li class="<?php echo $card['card_type']; ?>">
						<span class='card-name'>
							<?php echo $card['card_name']; ?>
						</span>
						<span class='card-type'>
							<?php echo $card['card_type']; ?>
						</span>
					</li>
					<?php }} ?>
				</ul>
			</div>
			<?php } if ($is_judge) { ?>
			<div class="seperator"></div>
			<div id="defend">
				<p class="description">
					<b>Defend: </b>
					<?php echo $game['game_defend_scenario']; ?>
				</p>
				<ul class="cards small">
					<?php
					foreach ($game_cards as $game_card) {
						if ($game_card['game_card_field_status'] == 2) {
							$card = $db -> get_card($game_card['card_id']);
					?>
					<li class="<?php echo $card['card_type']; ?>">
						<span class='card-name'>
							<?php echo $card['card_name']; ?>
						</span>
						<span class='card-type'>
							<?php echo $card['card_type']; ?>
						</span>
					</li>
					<?php }} ?>
				</ul>
			</div>
			<?php } ?>
		</div>

		<?php if ($this_is_attack_player || $this_is_defend_player) { ?>
		<div class="seperator"></div>
		<div id="description">
			<textarea class="empty" rows="8" name="scenario"></textarea>
		</div>
		<div id="action">
			<input class="button" type="submit" 
				value="<?php echo $action_name; ?>">
			<?php } if ($this_is_attack_player) { ?>
			<span id="or">or</span>
			<a class="button" href="play_throw_away_hand.php?
				attack_player_id=<?php echo $attack_player['user_id']; ?>&amp;
				game_id=<?php echo $gid; ?>">
				Throw away hand
			</a>
			<?php } else if ($this_is_defend_player) { ?>
			<span id="or">or</span>
			<a class="button" href="play_take_the_hit.php?
				attack_player_id=<?php echo $attack_player['user_id']; ?>&amp;
				defend_player_id=<?php echo $defend_player['user_id']; ?>&amp;
				game_id=<?php echo $gid; ?>">
				Take the hit
			</a>
			<?php } ?>
		</div>

	<?php if ($good_to_refresh) { ?>
	</div>
	<?php } ?>
		
	</div>

	<div class="clear"></div>

	<?php if ($this_player['game_user_lives'] > 0 ) { ?>
	<div class="seperator"></div>
	<div id="player-hand" 
		 class="<?php echo ($this_is_attack_player || $this_is_defend_player) 
			? 'active' : ''; ?>">
		<?php if ($good_to_refresh) { ?>
		<div id="player-hand-refresh" class="refresh">
		<?php } ?>
		<ul class="cards">
			<?php
			if (count($this_player_cards) > 0) {
				foreach ($this_player_cards as $card) {
					if ($this_is_attack_player || $this_is_defend_player) {
						echo "<li class='$card[card_type] playing'>";
						if (
							$card['card_type'] == 'mod'
							||
							$card['card_type'] == 'wild'
						) {
							echo "<input type='checkbox'
								name='card_$card[card_id]'>";
						} else {
							echo "<input type='radio' name='main_card'
								value='$card[card_id]'>";
						}
					} else {
						echo "<li class='$card[card_type]'>";
					}
					echo "<span class='card-name'>$card[card_name]</span>";
					echo "<span class='card-type'>$card[card_type]</span>";
					echo '</li>';
				}
			} else {
				echo "<span id='empty-hand'>Your hand is empty</span>";
			}
			?>
		</ul>
		<?php if ($good_to_refresh) { ?>
		</div>
		<?php } ?>
	</div>
	<?php } ?>

	</form>

</div>

<?php include('footer.php'); ?>
