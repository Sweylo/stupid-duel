<?php

class db extends mysqli {

    public function __construct() {
		parent::__construct(
			$GLOBALS['config']['mysql_host'],
			$GLOBALS['config']['mysql_user'],
			$GLOBALS['config']['mysql_password'],
			$GLOBALS['config']['mysql_db'],
			$GLOBALS['config']['mysql_port']
		);
    }

    public function select_all_from($table) {

        $sql = "SELECT * FROM $table";
        $result = mysqli_query($this, $sql);
        $results = array();

        while ($row = mysqli_fetch_assoc($result)) {
            array_push($results, $row);
        }

        return $results;

    }

    public function select_one_from($table, $column, $value) {

        $sql = "SELECT * FROM $table WHERE $column = '$value'";
        $result = mysqli_query($this, $sql);
		
		echo $this -> error;

        return mysqli_fetch_assoc($result);

    }

    public function select_many_from($table, $column, $value) {

        $sql = "SELECT * FROM $table WHERE $column = '$value'";
        $result = mysqli_query($this, $sql);
        $results = array();

        while ($row = mysqli_fetch_assoc($result)) {
            array_push($results, $row);
        }

        return $results;

    }
	
	public function select($sql) {
		return $this -> query($sql) -> fetch_all(MYSQLI_BOTH);
	}
	
	public function alter($sql) {
		return $this -> query($sql);
	}

	public function get_card($card_id) {

		$sql_card =

			"SELECT * FROM (

				SELECT card_id, item_name as card_name, 'item' as card_type
				FROM cards c
					JOIN card_items ci ON c.card_id = ci.item_card_id
						JOIN items i ON ci.item_id = i.item_id

			UNION

				SELECT card_id, mod_name as card_name, 'mod' as card_type
			    FROM cards c
					JOIN card_mods cm ON c.card_id = cm.mod_card_id
						JOIN mods m ON cm.mod_id = m.mod_id

			UNION

				SELECT card_id, god_name as card_name, 'god' as card_type
			    FROM cards c
					JOIN card_gods cg ON c.card_id = cg.god_card_id
						JOIN gods g ON cg.god_id = g.god_id

			UNION

				SELECT card_id, wild_name as card_name, 'wild' as card_type
			    FROM cards c
					JOIN card_wilds cw ON c.card_id = cw.wild_card_id
						JOIN wilds w ON cw.wild_id = w.wild_id

			) as result

			WHERE card_id = $card_id";

		$result_card = mysqli_query($this, $sql_card);

	    if ($row_card = mysqli_fetch_assoc($result_card)) {
			return $row_card;
	    } else {
			die("Unable to find card with id=$card_id");
		}

	}

};

?>
