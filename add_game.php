<?php

$user_check = true;
include('global.php');

$deck_id = $_POST['deck'];
$game_name = $_POST['name'];
$cards = $db -> select_many_from('cards', 'card_deck_id', $deck_id);
shuffle($cards);

print_r($_POST);

if ($_POST['password'] && $_POST['password'] == $_POST['confirm']) {
	$sql = "INSERT INTO games
				(game_name, owner_user_id, deck_id, game_password)
			VALUES
				('$game_name', $user[user_id], $deck_id, '$_POST[password]')";
} else if ($_POST['password'] && $_POST['password'] != $_POST['confirm']) {
	header("Location: create_game.php?error=no_match");
} else {
	$sql = "INSERT INTO games (game_name, owner_user_id, deck_id)
			VALUES ('$game_name', $user[user_id], $deck_id)";
}

echo "$sql";

if ($db -> query($sql) == false) {
	die('Unable to add game: ' . $db -> error);
}

$game_id = $db -> insert_id;

$sql = "INSERT INTO game_users (game_id, user_id, game_user_order)
		VALUES ($game_id, $user[user_id], 1)";

if ($db -> query($sql) == false) {
	die('Unable to add user to game: ' . $db -> error);
}

foreach ($cards as $key => $card) {

	$sql = "INSERT INTO game_cards (card_id, card_order_index, deck_id, game_id)
			VALUES ($card[card_id], $key, $deck_id, $game_id)";

	if ($db -> query($sql) == false) {
		die('Unable to link a card to the game: ' . $db -> error);
	}

}

header("Location: lobby.php?game_id=$game_id");

?>
