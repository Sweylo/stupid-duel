<?php

$user_check = true;
include('global.php');

$game_cards = $db -> select_many_from('game_cards', 'game_id', $_POST['game_id']);

if (!$_POST['main_card']) {
	header("Location: play.php?game_id=$_POST[game_id]&error=no_main_card");
	die();
}

if (!$_POST['scenario']) {
	header("Location: play.php?game_id=$_POST[game_id]&error=no_scenario");
	die();
}

foreach ($game_cards as $card) {
    if ($_POST["card_$card[card_id]"] == 'on') {

		$sql = "UPDATE game_cards
		        SET game_card_field_status = 2, user_hand_id = NULL
		        WHERE card_id = $card[card_id]
                    AND game_id = $_POST[game_id]";

		echo "$sql<br />";

		if ($db -> query($sql) == false) {
			die('Unable to update card: ' . $db -> error);
		}

    }
}

print_r($_POST);
echo '<br />';

$sql = "UPDATE games
        SET game_defend_scenario = '$_POST[scenario]'
        WHERE game_id = $_POST[game_id]";

echo "$sql<br />";

if ($db -> query($sql) == false) {
	die('Unable to update game: ' . $db -> error);
}

$sql = "UPDATE game_cards
        SET game_card_field_status = 2, user_hand_id = NULL
        WHERE card_id = $_POST[main_card]
            AND game_id = $_POST[game_id]";

echo "$sql<br />";

if ($db -> query($sql) == false) {
	die('Unable to update card: ' . $db -> error);
}

header("Location: play.php?game_id=$_POST[game_id]");

?>
