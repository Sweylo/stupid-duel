<?php

$user_check = true;   
include('global.php');

$decks = $db -> select_all_from('decks');
$games = $db -> select_all_from('games');

?>

<script>
	$(document).ready(function() {

		setInterval(function() {
			$('#decks').load('index.php #decks .refresh');
			$('#games').load('index.php #games .refresh');
		}, 3000);

	});
</script>

<div id="decks" class="content">

	<div id="decks-refresh" class="refresh">

		<div class="content-header">
			<h1>Decks</h1>
			<div class="clear"></div>
		</div>

		<table>
			<th>Name</th>
			<th>Creator</th>
			<?php
			foreach ($decks as $deck) {
				$link = "'edit_deck.php?deck_id=$deck[deck_id]'";
			?>
			<tr>
				<td>
					<a href=<?php echo $link; ?>>
						<?php echo $deck['deck_name']; ?>
					</a>
				</td>
				<td width="200px">
					<?php
					$sql = "SELECT *
							FROM decks JOIN users
								ON deck_creator_user_id = user_id
							WHERE deck_id = $deck[deck_id]";
					$result = mysqli_query($db, $sql);
					$row = mysqli_fetch_assoc($result);
					echo $row['user_name'];
					?>
				</td>
			</tr>
			<?php } ?>
		</table>

	</div>

</div>

<div id="games" class="content">

	<div id="games-refresh" class="refresh">

		<div class="content-header">
			<h1>Games</h1>
			<span id="error">
			<?php
				switch ($_GET['error']) {
					case 'gamenoexist':
						echo "Error - game#$_GET[game_id] went away";
						break;
				}
			?>
			</span>
			<div class="clear"></div>
		</div>

		<table>
			<th>Name</th>
			<th>Players</th>
			<th>Owner</th>
			<th>Status</th>
			<?php
			foreach ($games as $game) {
				$link = "'lobby.php?game_id=$game[game_id]'";
			?>
		    <tr>
		        <td>
		            <a href=<?php echo $link; ?>>
		                <?php echo $game['game_name']; ?>
					</a>
				</td>
				<td width="200px">
				<?php
				$sql = "SELECT COUNT(*) AS num_of_players
						FROM game_users gu JOIN users u
						ON gu.user_id = u.user_id
						WHERE game_id = $game[game_id]";
				$result = mysqli_query($db, $sql);
				$row = mysqli_fetch_assoc($result);
				echo $row['num_of_players'];
				?>
				</td>
				<td width="200px">
				<?php
				$sql = "SELECT *
						FROM users
						WHERE user_id = $game[owner_user_id]";
				$result = mysqli_query($db, $sql);
				$row = mysqli_fetch_assoc($result);
				echo $row['user_name'];
				?>
				</td>
				<td width="200px">
				<?php
				if ($game['game_turn'] > 0 && $game['game_over'] != 1) {
					echo 'In progress';
				} else if ($game['game_turn'] > 0 && $game['game_over'] == 1) {
					echo 'Game over';
				} else {
					echo 'Waiting for players to join';
				}
				?>
				</td>
			</tr>
			<?php } ?>
		</table>

	</div>

</div>

<?php include('footer.php'); ?>
