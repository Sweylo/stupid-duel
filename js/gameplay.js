$(document).ready(function () {

	function refresh() {

		var refreshItems = $('body').find('.refresh');

		for (var i = 0; i < refreshItems.length; i++) {

			var parent = $(refreshItems[i]).parent();
			var id = $(parent).attr("id");

			$('#' + id).load(
				"play.php?game_id=" + game_id + " #"
					+ $(refreshItems[i]).attr('id')
			, function() {
				if ($('#' + id).text() == '') {
					location.reload(true);
				}
			});

		}

	}

	autorefresh = setInterval(refresh, 2000);

	var text = 'Describe the scenario';
	$('textarea.empty').val(text);

	$(document).on('focusin', 'textarea', function () {
		if ($(this).val().trim() === text) {
			$(this).val('');
			$(this).removeClass('empty');
		}
	});

	$(document).on('focusout', 'textarea', function () {
		if ($(this).val().trim() === '') {
			$(this).val(text);
			$(this).addClass('empty');
		}
	});

	$(document).on('click', '#players tr input', function () {
		return false;
	});

	$(document).on('click', '.cards li, #players tr.targetable', function () {

		$(this).find('input').prop(
				'checked',
				!$(this).find('input').is(":checked")
				);

		var cards = $('#player-hand .cards').find('li');
		var players = $('#players').find('tr.targetable');

		for (var i = 0; i < cards.length; i++) {
			if ($(cards[i]).find('input').is(":checked")) {
				$(cards[i]).addClass('selected');
			} else {
				$(cards[i]).removeClass('selected');
			}
		}

		for (var i = 0; i < players.length; i++) {
			if ($(players[i]).find('input').is(":checked")) {
				$(players[i]).addClass('selected');
			} else {
				$(players[i]).removeClass('selected');
			}
		}

	});

	$(document).on('click', '#players tr input', function () {
		return false;
	});

});
