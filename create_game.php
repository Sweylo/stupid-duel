<?php

$user_check = true;
include('global.php');

$decks = $db -> select_all_from('decks');

?>

<form method="post" action="add_game.php">
		
<div class="content">

	<div class="content-header">
		<h1>Game Creator</h1>
		<input class="button start" type="submit" value="Create game">
		<div class="clear"></div>
	</div>
    <div><input type="text" name="name" placeholder="Game name"></div>

    <div>
        <div>Select a deck:</div>
        <select name="deck">
            <?php
            foreach ($decks as $deck) {
                echo "<option value='$deck[deck_id]'>$deck[deck_name]</option>";
            }
            ?>
        </select>
    </div>

    <div>
        <input type="password" name="password"
            placeholder="Game password (leave blank for none)"><br />
        <input type="password" name="confirm"
            placeholder="Confirm password">
    </div>
	
</div>

</form>

<?php include('footer.php'); ?>
