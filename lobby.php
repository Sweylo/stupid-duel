<?php

$user_check = true;
include('global.php');

$game = $db -> select_one_from('games', 'game_id', $_GET['game_id']);

if (!$game) {
	echo "<a href='index.php'>&laquo; Back</a><br />";
	die("Game (id#$_GET[id]) cannot be found.");
}

$deck = $db -> select_one_from('decks', 'deck_id', $game['deck_id']);

if ($game['game_turn'] > 0) {
	header("Location: play.php?game_id=$game[game_id]");
}

$sql = "SELECT *
		FROM users u JOIN game_users gu
			ON u.user_id = gu.user_id
		WHERE gu.game_id = $game[game_id]";
$result = mysqli_query($db, $sql);
$players = array();

while ($row = mysqli_fetch_assoc($result)) {
	if ($row['user_id'] == $user['user_id']) {
		$joined = true;
	}
	array_push($players, $row);
}

$is_owner = $user['user_id'] == $game['owner_user_id'];

?>

<script>
	$(document).ready(function() {

		function refresh() {

			var url = "lobby.php?game_id=<?php echo $game['game_id']; ?>";

			$('#player-list.refresh').load(url + " #player-list");
			$('#game-action.refresh').load(url + " #game-action", function() {
				if ($('#player-list').find('table').attr('id') == 'players') {
					location.reload(true);
				}
			});

		}

		autorefresh = setInterval(refresh, 2000);

		$(document).on('click', 'span#join', function() {
			$('#join-shadow').fadeIn(200);
		});

		$(document).on('click', '#join-close', function() {
			$('#join-shadow').fadeOut(200);
		});

	});
</script>

<div class="content">

	<form action="join_game.php?game_id=<?php echo $game['game_id']; ?>"
		method="post">

		<div class="content-header">

	    	<h1>Lobby - <?php echo $game['game_name']; ?></h1>

			<span id="deck">
				 Deck: <b><?php echo $deck['deck_name']; ?></b>
			</span>

			<div id="game-action" class="refresh">
		    <?php
			if (!$joined && !$is_owner && $user_perm != USER_GUEST) {
				/*echo '<span id="join">';
				if ($game['game_password']) {
					echo '<input name="password"
						placeholder="Game password">';
				}
				echo "<input id='join' type='submit'
					value='Join game'></span>";*/
				if ($game['game_password']) {
					echo "<span id='join' class='button'>Join</span>";
				} else {
					echo "<a id='join' class='button'
						href='join_game.php?game_id=$game[game_id]'>
						Join</a>";
				}
		    } else if ($joined && !$is_owner) {
		    	echo "<a class='button leave' href='leave_game.php
					?game_id=$game[game_id]'>Leave</a>";
		    } else if ($joined && $is_owner) {
				if (count($players) >= 3) {
					echo "<a class='button start' href='start_game.php
						?game_id=$game[game_id]'>Start</a>";
				}
				echo "<a class='button delete' href='game_delete.php
					?game_id=$game[game_id]'>Delete</a>";
			}
			?>

			</div>

			<div class="clear"></div>

		</div>

		<div id="player-list"
			<?php echo ($joined) ? 'class="refresh"' : ''; ?>>
		    <h2>Players</h2>
		    <ul>
		    <?php
		    foreach ($players as $player) {
				if ($player['user_id'] == $user['user_id']) {
					echo "<li><b>$player[user_name]</b></li>";
				} else {
					echo "<li>$player[user_name]</li>";
				}
		    }
		    ?>
		    </ul>
			<?php
			if (count($players) < 3) {
				echo "<p class='wait'>Waiting on at least "
					. (3 - count($players))
					. " more player" . (count($players) > 1 ? '' : 's')
					. " to start.</p>";
			} else {
				echo "<p class='wait'>Waiting for host to start the game.</p>";
			}
			?>
		</div>

		<div id="join-shadow" class="hidden">
			<div id="join-window">
				<h2>Enter the game password:</h2>
				<span id="join">
					<input name="password" placeholder="Game password">
					<input type='submit' value='Join game'>
				</span>
				<span id="join-close" class="button">X</span>
			</div>
		</div>

	</form>

</div>

<?php include('footer.php'); ?>
