<?php

$user_check = true;
include('global.php');

if ($user['user_permission_level'] < 2) {
	die("You don't have permission to create decks.");
}

$items = $db -> select_all_from('items');
$mods = $db -> select_all_from('mods');
$gods = $db -> select_all_from('gods');
$wilds = $db -> select_all_from('wilds');

?>

<form method="post" action="add_deck.php">

<div class="content">
	
	<div class="content-header">
		<h1>Deck Creator</h1>
		<input class="button start" type="submit" value="Create deck">
		<div class="clear"></div>
	</div>

    <div><input type="text" name="name" placeholder="Deck name"></div>

    <table id="cards">

        <tr>
            <th>Items</th>
            <th>Quantity</th>
        </tr>

        <?php
        foreach ($items as $item) {
            echo "<tr><td width='100%'>";
            echo "<input type='checkbox' "
					. "name='item_$item[item_id]' checked>$item[item_name]";
            echo "</td><td>";
            echo "<input type='number' "
					. "name='item_quantity_$item[item_id]' value='2'>";
            echo "</td></tr>\n";
        }
        ?>
		
		<tr>
			<td class="seperator"></td>
			<td class="seperator"></td>
		</tr>

        <tr>
            <th>Modifiers</th>
            <th>Quantity</th>
        </tr>

        <?php
        foreach ($mods as $mod) {
            echo "<tr><td>";
            echo "<input type='checkbox' "
					. "name='mod_$mod[mod_id]' checked>$mod[mod_name]";
            echo "</td><td>";
            echo "<input type='number' "
					. "name='mod_quantity_$mod[mod_id]' value='2'>";
            echo "</td></tr>\n";
        }
        ?>
		
		<tr>
			<td class="seperator"></td>
			<td class="seperator"></td>
		</tr>

        <tr>
            <th>Gods</th>
            <th>Quantity</th>
        </tr>

        <?php
        foreach ($gods as $key => $god) {
            echo "<tr><td>";
            echo "<input type='checkbox' "
					. "name='god_$god[god_id]' checked>$god[god_name]";
            echo "</td><td>";
            echo "<input type='number' "
					. "name='god_quantity_$god[god_id]' value='1'>";
            echo "</td></tr>\n";
        }
        ?>
		
		<tr>
			<td class="seperator"></td>
			<td class="seperator"></td>
		</tr>

        <tr>
            <th>Wilds</th>
            <th>Quantity</th>
        </tr>

        <?php
        foreach ($wilds as $key => $wild) {
            echo "<tr><td>";
            echo "<input type='checkbox' "
					. "name='wild_$wild[wild_id]' checked>$wild[wild_name]";
            echo "</td><td>";
            echo "<input type='number' "
					. "name='wild_quantity_$wild[wild_id]' value='1'>";
            echo "</td></tr>\n";
        }
        ?>

    </table>

</div>
	
</form>

<?php include('footer.php'); ?>
