<?php

$user_check = true;
include('global.php');

$game = $db -> select_one_from('games', 'game_id', $_GET['game_id']);

if (!$game) {
	die("Game (id#$_GET[game_id]) cannot be found.");
}

// determine if there's a winner and redirect if so
$sql = "SELECT *
		FROM users u JOIN game_users gu
			ON u.user_id = gu.user_id
		WHERE gu.game_id = $game[game_id] AND game_user_lives > 0
		ORDER BY gu.game_user_order";
$result = mysqli_query($db, $sql);
if ($result -> num_rows == 1) {

	// update the game to indicate the game's over

	$sql = "UPDATE games
	        SET game_over = 1
	        WHERE game_id = $game[game_id]";

	//echo "$sql<br />";

	if ($db -> query($sql) == false) {
		echo "1";
		die('Unable to update user: ' . $db -> error);
	}

	header("Location: game_end.php?game_id=$game[game_id]");
	die();
	
}

// get an array containing all players that are in the game
$players = $db -> select(
	"SELECT * "
	. "FROM users u JOIN game_users gu "
		. "ON u.user_id = gu.user_id "
	. "WHERE gu.game_id = $game[game_id]"
);

// determine which player's turn it is
$player_turn = ($game['game_turn'] % count($players) == 0) ?
	 count($players) : $game['game_turn'] % count($players);

// assign players to their respective roles
foreach ($players as $player) {
	if ($player['user_id'] == $user['user_id']) {
		$this_player = $player;
	}
	if ($player['game_user_order'] == $player_turn) {
		$attack_player = $player;
	}
	if ($player['user_id'] == $game['game_defender_id']) {
		$defend_player = $player;
	}
	$vote += $player['game_user_vote'];
}

$toss = (mt_rand(0, 1) == 1) ? 'heads' : 'tails';
	
// see if the coin toss is called correctly and adjust accordingly
if ($game['game_coin_toss']) {
	$vote += ($_POST['call'] == $toss) ? 1 : -1;
}

// perform coin toss if there is a tie in the votes
if ($vote == 0 && !$game['game_coin_toss']) {
	
	$db -> alter(
		"UPDATE games "
		. "SET game_coin_toss = 1 "
		. "WHERE game_id = $game[game_id]"
	) or die('2Unable to update game: ' . $db -> error);

	header("Location: play.php?game_id=$game[game_id]");
	die();
	
// proceed when coin toss has been done	
} else if (($vote == 0 && $game['game_coin_toss']) || $vote < 0) { 

	// decrement defending player's life count
	$lives = $defend_player['game_user_lives'] - 1;
	$db -> alter(
		"UPDATE game_users "
		. "SET game_user_lives = $lives "
		. "WHERE game_id = $game[game_id] "
			. "AND user_id = $defend_player[user_id]"
	) or die('3Unable to update game_users: ' . $db -> error);

	// if the player is now out of the game
	if ($lives == 0) {

		// send all cards from a defeated player's hand to the discard pile
		$db -> alter(
			"UPDATE game_cards "
			. "SET user_hand_id = NULL, game_card_field_status = -1 "
			. "WHERE game_id = $game[game_id] "
				. "AND user_hand_id = $defend_player[user_id]"
		) or die('4Unable to update game_cards: ' . $db -> error);

	}

}

// set votes back to 0
$db -> alter(
	"UPDATE game_users "
	. "SET game_user_vote = 0 "
	. "WHERE game_id = $game[game_id]"
) or die('5Unable to update game_users: ' . $db -> error);

// set scenarios back to null and increments the turn counter
$turn = $game['game_turn'] + 1;
$db -> alter(
	"UPDATE games "
	. "SET game_attack_scenario = NULL, "
		. "game_defend_scenario = NULL, "
		. "game_defender_id = NULL, "
		. "game_turn = $turn, "
		. "game_coin_toss = 0 "
	. "WHERE game_id = $game[game_id]"
) or die('6Unable to update game: ' . $db -> error);

// put played cards in the discard pile
$db -> alter(
	"UPDATE game_cards "
	. "SET game_card_field_status = -1 "
	. "WHERE game_id = $game[game_id] "
		. "AND game_card_field_status > 0"
) or die('7Unable to update game_cards: ' . $db -> error);

// get number of cards needed to complete the attacker's hand
$sql = "SELECT COUNT(user_hand_id) AS num_cards_needed
		FROM game_cards
		WHERE user_hand_id = $attack_player[user_id]
			AND game_id = $game[game_id]";
$result = mysqli_query($db, $sql);
if ($row = mysqli_fetch_assoc($result)) {
	$needed = 5 - $row['num_cards_needed'];
}

echo "needed = $needed<br />";

// get number of cards remaining in the deck
/*$remaining_query = $db -> select(
	"SELECT COUNT(*) AS num_cards_remaining "
	. "FROM game_cards "
	. "WHERE user_hand_id IS NULL "
		. "AND game_card_field_status = 0 "
		. "AND game_id = $game[game_id]"
);
$remaining = $remaining_query['num_cards_remaining'];*/
$sql = "SELECT COUNT(game_card_id) AS num_cards_remaining
		FROM game_cards
		WHERE user_hand_id IS NULL
			AND game_card_field_status = 0
			AND game_id = $game[game_id]";
$result = mysqli_query($db, $sql);

//echo "$sql<br />";

if ($row = mysqli_fetch_assoc($result)) {
	$remaining = $row['num_cards_remaining'];
}

echo "remaining = $remaining<br />";

// get the largest card order index
/*$max_index_query = $db -> select(
	"SELECT MAX(card_order_index) AS max_card_order_index "
	. "FROM game_cards "
	. "WHERE game_id = $game[game_id]"
);
$max_card_order_index = $max_index_query['max_card_order_index'];*/
$sql = "SELECT MAX(card_order_index) AS max_card_order_index
		FROM game_cards
		WHERE game_id = $game[game_id]";
$result = mysqli_query($db, $sql);

//echo "$sql<br />";

if ($row = mysqli_fetch_assoc($result)) {
	$max_card_order_index = $row['max_card_order_index'];
}

echo "max = $max_card_order_index<br />";

if ($remaining < 5) {
	
	echo 'reshuffling discard deck';

	// get cards from the discard pile
	$discard_pile = $db -> select(
		"SELECT "
			. "COUNT(game_card_id) AS num_cards_remaining, "
			. "MAX(card_order_index) AS max_card_order_index, "
			. "game_card_id "
		. "FROM game_cards "
		. "WHERE game_id = $game[game_id] "
			. "AND game_card_field_status = -1"
	);
	
	// shuffle the discard pile
	shuffle($discard_pile);

	// update each discarded card to its new shuffled order index
	foreach ($discard_pile as $card) {

		//print_r($card);

		$max_card_order_index++;
		
		$db -> alter(
			"UPDATE game_cards "
			. "SET game_card_field_status = 0, "
				. "card_order_index = $max_card_order_index "
			. "WHERE game_id = $game[game_id] "
				. "AND game_card_id = $card[game_card_id]"
		) or die('8Unable to update game_cards: ' . $db -> error);

	}

}

// get the specified number of cards from the deck
echo "SELECT * "
	. "FROM game_cards "
	. "WHERE game_id = $game[game_id] "
		. "AND user_hand_id IS NULL "
		. "AND game_card_field_status = 0 "
	. "ORDER BY card_order_index "
	. "LIMIT $needed";
$new_cards = $db -> select(
	"SELECT * "
	. "FROM game_cards "
	. "WHERE game_id = $game[game_id] "
		. "AND user_hand_id IS NULL "
		. "AND game_card_field_status = 0 "
	. "ORDER BY card_order_index "
	. "LIMIT $needed"
);
foreach ($new_cards as $card) {
	// assign the new card to the attacking player
	$db -> alter(
		"UPDATE game_cards "
		. "SET user_hand_id = $attack_player[user_id] "
		. "WHERE game_id = $game[game_id] "
			. "AND game_card_id = $card[game_card_id]"
	) or die('9Unable to update game_cards: ' . $db -> error);
}

header("Location: play.php?game_id=$game[game_id]");
die();

?>