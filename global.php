<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set("display_errors", 1);
date_default_timezone_set('UTC');

// read in settings from the config file
$GLOBALS['config'] = json_decode(file_get_contents('config.json'), true)
	or header("Location: create_config.php");

// include sql class and instantiate an sql object
include('db.php');
$db = new db();

// define user permission level constants
const USER_GUEST = 0;
const USER_PLEB = 1;
const USER_MOD = 2;
const USER_ADMIN = 3;
const USER_OP = 4;

if ($user_check) {
	if (!isset($_COOKIE['user']) || !isset($_COOKIE['login_session'])) {
		if ($GLOBALS['config']['enable_guest']) {
			$user = $db -> select_one_from('users', 'user_name', 'guest');
		} else {
			header('Location: login.php');
		}
	} else {
		$user = $db -> select_one_from('users', 'user_name', $_COOKIE['user']);
		$user_perm = $user['user_permission_level'];
		if ($user['user_login_session'] != $_COOKIE['login_session']) {
			header('Location: logout.php');
		}
	}
} else {
	echo '<style>#header { display: none; }</style>';
}

?>

<!DOCTYPE html>

<head>
	<title>Stupid-Duel - <?php echo $user['user_name']; ?></title>
	<link rel="stylesheet" type="text/css" href="css/reset.css" />
	<link rel="stylesheet" type="text/css" href="css/fonts.css" />
	<link rel="stylesheet" type="text/css" href="css/main.css" />
	<script src="js/jquery-3.1.1.min.js"></script>
</head>

<body>

<div id="header">

	<h1>
		<a href="index.php">
			Stupid-Duel
			<img id="home-img" width="20px" src="img/home.png" />
		</a>
	</h1>

    <ul id="nav">
        <li><a href="create_card.php">Card Creator</a></li>
        <li><a href="create_deck.php">Deck Creator</a></li>
        <li><a href="create_game.php">Game Creator</a></li>
		<?php if ($user_perm != USER_GUEST) { ?>
		<li id="account" class="dropdown">
			<span><?php echo $user['user_name']; ?></span>
			<div class="arrow-down"></div>
			<ul>
				<li>
				<?php
				switch ($user_perm) {
					case USER_PLEB: 
					case USER_MOD:
						echo "<a href='user_options.php'>User options</a>";
						break;
					case USER_ADMIN:
					case USER_OP:
						echo "<a href='admin_panel.php'>Admin panel</a>";
						break;
				} 
				?>
				</li>
				<li><a href="logout.php">Logout</a></li>
			</ul>
		</li>
		<?php } else { ?>
		<li id="login" class="dropdown">
			<form action="authenticate_user.php" method="post">
				<span>Login</span>
				<div class="arrow-down"></div>
				<ul>
					<li>
						<input 
							type="text" 
							name="username" 
							placeholder="Username">
					</li>
					<li>
						<input 
							type="password" 
							name="password" 
							placeholder="Password">
					</li>
					<li><input type="submit" value="Login"></li>
				</ul>
			</form>
		</li>
		<?php } ?>
    </ul>

	<div class="clear"></div>

</div>
