<?php

$user_check = true;
include('global.php');

$types = array('item', 'mod', 'god', 'wild');

$deck_name = $_POST['name'];
$sql = "INSERT INTO decks (deck_name, deck_creator_user_id)"
    .   " VALUES ('$deck_name', $user[user_id])";
if ($db -> query($sql) == true) {
    $deck_id = $db -> insert_id;
} else {
    echo "<b>Error adding deck:</b> <em>" . $db -> error . "</em>";
    die();
}

foreach ($types as $type) {

    $sql = "SELECT max(${type}_id) as ${type}_id_max FROM ${type}s";
    $result = mysqli_query($db, $sql);
    $types = array();

    if ($row = mysqli_fetch_assoc($result)) {
        $type_id_max = $row["${type}_id_max"];
    } else {
        die($db -> error);
    }

    echo "${type}_id_max = $type_id_max <br />";

    $sql = "SELECT max(card_id) as card_id_max FROM cards";
    $result = mysqli_query($db, $sql);
    $types = array();

    if ($row = mysqli_fetch_assoc($result)) {
        $card_id_max = ($row["card_id_max"]) ? $row["card_id_max"] : 0;
    } else {
        die($db -> error);
    }

    echo "card_id_max = $card_id_max <br />";

	$sql = "ALTER TABLE cards AUTO_INCREMENT = $card_id_max";
	if ($db -> query($sql) == false) {
	    die($db -> error);
	}

    $row_count = 1;
    $sql_card = "INSERT INTO cards (card_deck_id) VALUES ";
    $sql_link = "INSERT INTO card_${type}s (${type}_id, ${type}_card_id) VALUES ";

    for ($type_id = 1; $type_id <= $type_id_max; $type_id++) {
        if ($_POST["${type}_$type_id"] == 'on') {
            for ($i = 0; $i < $_POST["${type}_quantity_$type_id"]; $i++) {

                $card_id = $card_id_max + $row_count;

                $sql_card .= "($deck_id), ";
                $sql_link .= "($type_id, $card_id), ";

                $row_count++;

            }
        }
    }

	$sql_card = substr(trim($sql_card), 0, strlen(trim($sql_card)) - 1);
	$sql_link = substr(trim($sql_link), 0, strlen(trim($sql_link)) - 1);

    echo $sql_card . '<br />' . $sql_link . '<br />';

    if ($db -> query($sql_card) == true) {
        if ($db -> query($sql_link) == true) {
            header("Location: index.php");
        } else {
            die($db -> error);
        }
    } else {
        die($db -> error);
    }

}

?>
