INSERT INTO mods
	(mod_name)
VALUES
	('electric');
    
INSERT INTO card_mods
	(mod_id)
VALUES
	(1);
    
-- gets information from appropriate tables based on card_id

SELECT * FROM (

	SELECT card_id, item_name as card_name, 'item' as card_type
	FROM cards c
		JOIN card_items ci ON c.card_id = ci.item_card_id
			JOIN items i ON ci.item_id = i.item_id
            
UNION
    
	SELECT card_id, mod_name as card_name, 'mod' as card_type
    FROM cards c
		JOIN card_mods cm ON c.card_id = cm.mod_card_id
			JOIN mods m ON cm.mod_id = m.mod_id 
            
UNION
    
	SELECT card_id, god_name as card_name, 'god' as card_type
    FROM cards c
		JOIN card_gods cg ON c.card_id = cg.god_card_id
			JOIN gods g ON cg.god_id = g.god_id
            
UNION
    
	SELECT card_id, wild_name as card_name, 'wild' as card_type
    FROM cards c
		JOIN card_wilds cw ON c.card_id = cw.wild_card_id
			JOIN wilds w ON cw.wild_id = w.wild_id
            
) as result
WHERE card_id = 230;

SELECT max(item_id) as item_id_max FROM items;