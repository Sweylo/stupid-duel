-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema stupid-duel
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema stupid-duel
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `stupid-duel` DEFAULT CHARACTER SET utf8 ;
USE `stupid-duel` ;

-- -----------------------------------------------------
-- Table `stupid-duel`.`decks`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stupid-duel`.`decks` (
  `deck_id` INT NOT NULL AUTO_INCREMENT,
  `deck_name` VARCHAR(100) NOT NULL DEFAULT 'new deck',
  `deck_creator_user_id` INT NOT NULL,
  PRIMARY KEY (`deck_id`),
  UNIQUE INDEX `creator_id_UNIQUE` (`deck_creator_user_id` ASC),
  CONSTRAINT `deck_creator_user_id`
    FOREIGN KEY (`deck_creator_user_id`)
    REFERENCES `stupid-duel`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stupid-duel`.`games`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stupid-duel`.`games` (
  `game_id` INT NOT NULL AUTO_INCREMENT,
  `game_name` VARCHAR(100) NULL DEFAULT 'stupid-duel',
  `owner_user_id` INT NOT NULL,
  `deck_id` INT NOT NULL,
  PRIMARY KEY (`game_id`),
  INDEX `owner_user_id_idx` (`owner_user_id` ASC),
  INDEX `deck_id_idx` (`deck_id` ASC),
  CONSTRAINT `owner_user_id`
    FOREIGN KEY (`owner_user_id`)
    REFERENCES `stupid-duel`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `deck_id`
    FOREIGN KEY (`deck_id`)
    REFERENCES `stupid-duel`.`decks` (`deck_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stupid-duel`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stupid-duel`.`users` (
  `user_id` INT NOT NULL AUTO_INCREMENT,
  `user_name` VARCHAR(100) NOT NULL,
  `user_password` VARCHAR(100) NOT NULL,
  `user_permission_level` INT NOT NULL DEFAULT 1,
  `game_id` INT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `user_name_UNIQUE` (`user_name` ASC),
  UNIQUE INDEX `user_password_UNIQUE` (`user_password` ASC),
  INDEX `game_id_idx` (`game_id` ASC),
  CONSTRAINT `game_id`
    FOREIGN KEY (`game_id`)
    REFERENCES `stupid-duel`.`games` (`game_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stupid-duel`.`items`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stupid-duel`.`items` (
  `item_id` INT NOT NULL AUTO_INCREMENT,
  `item_name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`item_id`),
  UNIQUE INDEX `item_name_UNIQUE` (`item_name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stupid-duel`.`mods`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stupid-duel`.`mods` (
  `mods_id` INT NOT NULL AUTO_INCREMENT,
  `mods_name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`mods_id`),
  UNIQUE INDEX `modifier_name_UNIQUE` (`mods_name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stupid-duel`.`cards`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stupid-duel`.`cards` (
  `card_id` INT NOT NULL AUTO_INCREMENT,
  `card_creator_user_id` INT NOT NULL,
  PRIMARY KEY (`card_id`),
  INDEX `creator_user_id_idx` (`card_creator_user_id` ASC),
  CONSTRAINT `card_creator_user_id`
    FOREIGN KEY (`card_creator_user_id`)
    REFERENCES `stupid-duel`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stupid-duel`.`card_items`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stupid-duel`.`card_items` (
  `card_item_id` INT NOT NULL AUTO_INCREMENT,
  `item_id` INT NOT NULL,
  `item_card_id` INT NOT NULL,
  PRIMARY KEY (`card_item_id`),
  UNIQUE INDEX `item_id_UNIQUE` (`item_id` ASC),
  UNIQUE INDEX `deck_id_UNIQUE` (`item_card_id` ASC),
  CONSTRAINT `item_card_id`
    FOREIGN KEY (`item_card_id`)
    REFERENCES `stupid-duel`.`cards` (`card_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `item_id`
    FOREIGN KEY (`item_id`)
    REFERENCES `stupid-duel`.`items` (`item_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stupid-duel`.`card_mods`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stupid-duel`.`card_mods` (
  `card_mod_id` INT NOT NULL AUTO_INCREMENT,
  `mod_id` INT NOT NULL,
  `mod_card_id` INT NOT NULL,
  PRIMARY KEY (`card_mod_id`),
  UNIQUE INDEX `item_id_UNIQUE` (`mod_id` ASC),
  UNIQUE INDEX `deck_id_UNIQUE` (`mod_card_id` ASC),
  CONSTRAINT `mod_id`
    FOREIGN KEY (`mod_id`)
    REFERENCES `stupid-duel`.`mods` (`mods_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `mod_card_id`
    FOREIGN KEY (`mod_card_id`)
    REFERENCES `stupid-duel`.`cards` (`card_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stupid-duel`.`gods`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stupid-duel`.`gods` (
  `god_id` INT NOT NULL AUTO_INCREMENT,
  `god_name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`god_id`),
  UNIQUE INDEX `god_name_UNIQUE` (`god_name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stupid-duel`.`card_gods`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stupid-duel`.`card_gods` (
  `card_god_id` INT NOT NULL AUTO_INCREMENT,
  `god_id` INT NOT NULL,
  `god_card_id` INT NOT NULL,
  PRIMARY KEY (`card_god_id`),
  UNIQUE INDEX `god_id_UNIQUE` (`god_id` ASC),
  UNIQUE INDEX `deck_id_UNIQUE` (`god_card_id` ASC),
  CONSTRAINT `god_id`
    FOREIGN KEY (`god_id`)
    REFERENCES `stupid-duel`.`gods` (`god_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `god_card_id`
    FOREIGN KEY (`god_card_id`)
    REFERENCES `stupid-duel`.`cards` (`card_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stupid-duel`.`deck_cards`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stupid-duel`.`deck_cards` (
  `deck_card_id` INT NOT NULL AUTO_INCREMENT,
  `card_id` INT NOT NULL,
  `card_deck_id` INT NOT NULL,
  `user_hand_id` INT NULL,
  `deck_card_discard` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`deck_card_id`),
  UNIQUE INDEX `card_id_UNIQUE` (`card_ref_id` ASC),
  UNIQUE INDEX `deck_id_UNIQUE` (`card_deck_id` ASC),
  INDEX `user_id_idx` (`user_hand_id` ASC),
  CONSTRAINT `card_ref_id`
    FOREIGN KEY (`card_ref_id`)
    REFERENCES `stupid-duel`.`cards` (`card_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `card_deck_id`
    FOREIGN KEY (`card_deck_id`)
    REFERENCES `stupid-duel`.`decks` (`deck_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `user_hand_id`
    FOREIGN KEY (`user_hand_id`)
    REFERENCES `stupid-duel`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'If user_id is null and deck_card_discard is 0, then the card is in the deck.';


-- -----------------------------------------------------
-- Table `stupid-duel`.`wildcards`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stupid-duel`.`wildcards` (
  `wildcard_id` INT NOT NULL AUTO_INCREMENT,
  `wildcard_name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`wildcard_id`),
  UNIQUE INDEX `wildcard_name_UNIQUE` (`wildcard_name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `stupid-duel`.`card_wildcard`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `stupid-duel`.`card_wildcard` (
  `card_wildcard_id` INT NOT NULL AUTO_INCREMENT,
  `wildcard_id` INT NOT NULL,
  `wildcard_card_id` INT NOT NULL,
  PRIMARY KEY (`card_wildcard_id`),
  UNIQUE INDEX `wildcard_id_UNIQUE` (`wildcard_id` ASC),
  UNIQUE INDEX `card_id_UNIQUE` (`wildcard_card_id` ASC),
  CONSTRAINT `wildcard_id`
    FOREIGN KEY (`wildcard_id`)
    REFERENCES `stupid-duel`.`wildcards` (`wildcard_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `wildcard_card_id`
    FOREIGN KEY (`wildcard_card_id`)
    REFERENCES `stupid-duel`.`cards` (`card_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
