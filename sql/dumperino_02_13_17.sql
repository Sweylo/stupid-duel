-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: sweylo.net    Database: stupid-duel
-- ------------------------------------------------------
-- Server version	5.6.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `card_gods`
--

DROP TABLE IF EXISTS `card_gods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_gods` (
  `card_god_id` int(11) NOT NULL AUTO_INCREMENT,
  `god_id` int(11) NOT NULL,
  `god_card_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`card_god_id`),
  UNIQUE KEY `god_card_id_UNIQUE` (`god_card_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_gods`
--

LOCK TABLES `card_gods` WRITE;
/*!40000 ALTER TABLE `card_gods` DISABLE KEYS */;
INSERT INTO `card_gods` VALUES (12,1,257),(13,2,258),(14,3,259),(15,4,260),(16,5,261);
/*!40000 ALTER TABLE `card_gods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card_items`
--

DROP TABLE IF EXISTS `card_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_items` (
  `card_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `item_card_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`card_item_id`),
  UNIQUE KEY `item_card_id_UNIQUE` (`item_card_id`)
) ENGINE=InnoDB AUTO_INCREMENT=937 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_items`
--

LOCK TABLES `card_items` WRITE;
/*!40000 ALTER TABLE `card_items` DISABLE KEYS */;
INSERT INTO `card_items` VALUES (159,2,171),(160,2,172),(161,7,173),(162,7,174),(163,8,175),(164,8,176),(165,9,177),(166,9,178),(167,11,179),(168,11,180),(169,12,181),(170,12,182),(171,13,183),(172,13,184),(173,14,185),(174,14,186),(175,15,187),(176,15,188),(177,16,189),(178,16,190),(179,17,191),(180,17,192),(181,18,193),(182,18,194),(183,19,195),(184,19,196),(185,20,197),(186,20,198),(187,21,199),(188,21,200),(189,22,201),(190,22,202),(191,23,203),(192,23,204),(193,24,205),(194,24,206),(195,25,207),(196,25,208),(197,26,209),(198,26,210),(199,27,211),(200,27,212),(201,28,213),(202,28,214),(203,29,215),(204,29,216),(205,30,217),(206,30,218),(207,31,219),(208,31,220),(209,32,221),(210,32,222),(885,2,264),(886,2,265),(887,7,266),(888,7,267),(889,8,268),(890,8,269),(891,9,270),(892,9,271),(893,11,272),(894,11,273),(895,12,274),(896,12,275),(897,13,276),(898,13,277),(899,14,278),(900,14,279),(901,15,280),(902,15,281),(903,16,282),(904,16,283),(905,17,284),(906,17,285),(907,18,286),(908,18,287),(909,19,288),(910,19,289),(911,20,290),(912,20,291),(913,21,292),(914,21,293),(915,22,294),(916,22,295),(917,23,296),(918,23,297),(919,24,298),(920,24,299),(921,25,300),(922,25,301),(923,26,302),(924,26,303),(925,27,304),(926,27,305),(927,28,306),(928,28,307),(929,29,308),(930,29,309),(931,30,310),(932,30,311),(933,31,312),(934,31,313),(935,32,314),(936,32,315);
/*!40000 ALTER TABLE `card_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card_mods`
--

DROP TABLE IF EXISTS `card_mods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_mods` (
  `card_mod_id` int(11) NOT NULL AUTO_INCREMENT,
  `mod_id` int(11) NOT NULL,
  `mod_card_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`card_mod_id`),
  UNIQUE KEY `mod_card_id_UNIQUE` (`mod_card_id`)
) ENGINE=InnoDB AUTO_INCREMENT=445 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_mods`
--

LOCK TABLES `card_mods` WRITE;
/*!40000 ALTER TABLE `card_mods` DISABLE KEYS */;
INSERT INTO `card_mods` VALUES (3,2,223),(4,2,224),(5,3,225),(6,3,226),(7,4,227),(8,4,228),(9,5,229),(10,5,230),(11,6,231),(12,6,232),(13,7,233),(14,7,234),(15,8,235),(16,8,236),(17,9,237),(18,9,238),(19,10,239),(20,10,240),(21,11,241),(22,11,242),(23,12,243),(24,12,244),(25,13,245),(26,13,246),(27,14,247),(28,14,248),(29,15,249),(30,15,250),(31,16,251),(32,16,252),(33,17,253),(34,17,254),(35,18,255),(36,18,256),(411,2,316),(412,2,317),(413,3,318),(414,3,319),(415,4,320),(416,4,321),(417,5,322),(418,5,323),(419,6,324),(420,6,325),(421,7,326),(422,7,327),(423,8,328),(424,8,329),(425,9,330),(426,9,331),(427,10,332),(428,10,333),(429,11,334),(430,11,335),(431,12,336),(432,12,337),(433,13,338),(434,13,339),(435,14,340),(436,14,341),(437,15,342),(438,15,343),(439,16,344),(440,16,345),(441,17,346),(442,17,347),(443,18,348),(444,18,349);
/*!40000 ALTER TABLE `card_mods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card_wilds`
--

DROP TABLE IF EXISTS `card_wilds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_wilds` (
  `card_wild_id` int(11) NOT NULL AUTO_INCREMENT,
  `wild_id` int(11) NOT NULL,
  `wild_card_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`card_wild_id`),
  UNIQUE KEY `wild_card_id_UNIQUE` (`wild_card_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_wilds`
--

LOCK TABLES `card_wilds` WRITE;
/*!40000 ALTER TABLE `card_wilds` DISABLE KEYS */;
INSERT INTO `card_wilds` VALUES (1,1,262),(2,2,263);
/*!40000 ALTER TABLE `card_wilds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cards`
--

DROP TABLE IF EXISTS `cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cards` (
  `card_id` int(11) NOT NULL AUTO_INCREMENT,
  `card_deck_id` int(11) NOT NULL,
  PRIMARY KEY (`card_id`),
  KEY `card_deck_id_idx` (`card_deck_id`),
  CONSTRAINT `card_deck_id` FOREIGN KEY (`card_deck_id`) REFERENCES `decks` (`deck_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=350 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cards`
--

LOCK TABLES `cards` WRITE;
/*!40000 ALTER TABLE `cards` DISABLE KEYS */;
INSERT INTO `cards` VALUES (171,32),(172,32),(173,32),(174,32),(175,32),(176,32),(177,32),(178,32),(179,32),(180,32),(181,32),(182,32),(183,32),(184,32),(185,32),(186,32),(187,32),(188,32),(189,32),(190,32),(191,32),(192,32),(193,32),(194,32),(195,32),(196,32),(197,32),(198,32),(199,32),(200,32),(201,32),(202,32),(203,32),(204,32),(205,32),(206,32),(207,32),(208,32),(209,32),(210,32),(211,32),(212,32),(213,32),(214,32),(215,32),(216,32),(217,32),(218,32),(219,32),(220,32),(221,32),(222,32),(223,32),(224,32),(225,32),(226,32),(227,32),(228,32),(229,32),(230,32),(231,32),(232,32),(233,32),(234,32),(235,32),(236,32),(237,32),(238,32),(239,32),(240,32),(241,32),(242,32),(243,32),(244,32),(245,32),(246,32),(247,32),(248,32),(249,32),(250,32),(251,32),(252,32),(253,32),(254,32),(255,32),(256,32),(257,32),(258,32),(259,32),(260,32),(261,32),(262,32),(263,32),(264,48),(265,48),(266,48),(267,48),(268,48),(269,48),(270,48),(271,48),(272,48),(273,48),(274,48),(275,48),(276,48),(277,48),(278,48),(279,48),(280,48),(281,48),(282,48),(283,48),(284,48),(285,48),(286,48),(287,48),(288,48),(289,48),(290,48),(291,48),(292,48),(293,48),(294,48),(295,48),(296,48),(297,48),(298,48),(299,48),(300,48),(301,48),(302,48),(303,48),(304,48),(305,48),(306,48),(307,48),(308,48),(309,48),(310,48),(311,48),(312,48),(313,48),(314,48),(315,48),(316,48),(317,48),(318,48),(319,48),(320,48),(321,48),(322,48),(323,48),(324,48),(325,48),(326,48),(327,48),(328,48),(329,48),(330,48),(331,48),(332,48),(333,48),(334,48),(335,48),(336,48),(337,48),(338,48),(339,48),(340,48),(341,48),(342,48),(343,48),(344,48),(345,48),(346,48),(347,48),(348,48),(349,48);
/*!40000 ALTER TABLE `cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `decks`
--

DROP TABLE IF EXISTS `decks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `decks` (
  `deck_id` int(11) NOT NULL AUTO_INCREMENT,
  `deck_name` varchar(100) NOT NULL DEFAULT 'new deck',
  `deck_creator_user_id` int(11) NOT NULL,
  PRIMARY KEY (`deck_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `decks`
--

LOCK TABLES `decks` WRITE;
/*!40000 ALTER TABLE `decks` DISABLE KEYS */;
INSERT INTO `decks` VALUES (32,'standard',1),(48,'standard_nogods_nowilds',1);
/*!40000 ALTER TABLE `decks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_cards`
--

DROP TABLE IF EXISTS `game_cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_cards` (
  `game_card_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'This is a table to store the shuffled order of cards in the game.',
  `game_id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `card_order_index` int(11) NOT NULL,
  `deck_id` int(11) NOT NULL,
  `user_hand_id` int(11) DEFAULT NULL,
  `game_card_field_status` int(11) NOT NULL DEFAULT '0' COMMENT 'null if not on field, 1 if is on attack side, 2 if is on defend side, -1 for a being in discard',
  PRIMARY KEY (`game_card_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1024 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_cards`
--

LOCK TABLES `game_cards` WRITE;
/*!40000 ALTER TABLE `game_cards` DISABLE KEYS */;
INSERT INTO `game_cards` VALUES (838,13,223,43,32,NULL,0),(839,13,179,84,32,NULL,0),(840,13,235,74,32,NULL,0),(841,13,253,48,32,NULL,0),(842,13,190,59,32,NULL,0),(843,13,187,57,32,NULL,0),(844,13,183,24,32,NULL,0),(845,13,201,38,32,NULL,0),(846,13,213,73,32,NULL,0),(847,13,202,30,32,NULL,0),(848,13,180,80,32,NULL,0),(849,13,184,26,32,NULL,0),(850,13,239,34,32,NULL,0),(851,13,238,50,32,NULL,0),(852,13,171,77,32,NULL,0),(853,13,173,67,32,NULL,0),(854,13,178,89,32,NULL,0),(855,13,191,15,32,NULL,0),(856,13,220,44,32,NULL,0),(857,13,177,23,32,NULL,0),(858,13,224,54,32,NULL,0),(859,13,241,1,32,NULL,0),(860,13,245,90,32,NULL,0),(861,13,209,14,32,NULL,0),(862,13,229,85,32,NULL,0),(863,13,188,56,32,NULL,0),(864,13,195,39,32,NULL,0),(865,13,193,10,32,NULL,0),(866,13,246,37,32,NULL,0),(867,13,196,0,32,NULL,0),(868,13,175,12,32,NULL,0),(869,13,247,82,32,NULL,0),(870,13,257,87,32,NULL,0),(871,13,250,16,32,NULL,0),(872,13,234,40,32,NULL,0),(873,13,215,61,32,NULL,0),(874,13,225,91,32,NULL,0),(875,13,249,86,32,NULL,0),(876,13,214,88,32,NULL,0),(877,13,172,65,32,NULL,0),(878,13,233,83,32,NULL,0),(879,13,203,45,32,NULL,0),(880,13,219,27,32,NULL,0),(881,13,228,5,32,NULL,0),(882,13,200,58,32,NULL,0),(883,13,216,70,32,NULL,0),(884,13,242,46,32,NULL,0),(885,13,230,35,32,NULL,0),(886,13,252,4,32,NULL,0),(887,13,185,20,32,NULL,0),(888,13,176,18,32,NULL,0),(889,13,197,17,32,NULL,0),(890,13,210,62,32,NULL,0),(891,13,182,78,32,NULL,0),(892,13,258,64,32,NULL,0),(893,13,218,29,32,NULL,0),(894,13,174,53,32,NULL,0),(895,13,254,31,32,NULL,0),(896,13,232,25,32,NULL,0),(897,13,262,71,32,NULL,0),(898,13,236,49,32,NULL,0),(899,13,206,52,32,NULL,0),(900,13,194,7,32,NULL,0),(901,13,189,68,32,NULL,0),(902,13,222,81,32,NULL,0),(903,13,208,51,32,NULL,0),(904,13,207,72,32,NULL,0),(905,13,255,13,32,NULL,0),(906,13,205,11,32,NULL,0),(907,13,192,92,32,NULL,0),(908,13,226,47,32,NULL,0),(909,13,198,69,32,NULL,0),(910,13,181,41,32,NULL,0),(911,13,261,19,32,NULL,0),(912,13,186,8,32,NULL,0),(913,13,256,2,32,NULL,0),(914,13,248,21,32,NULL,0),(915,13,221,66,32,NULL,0),(916,13,211,3,32,NULL,0),(917,13,251,33,32,NULL,0),(918,13,204,55,32,NULL,0),(919,13,237,76,32,NULL,0),(920,13,240,9,32,NULL,0),(921,13,217,60,32,NULL,0),(922,13,199,42,32,NULL,0),(923,13,259,79,32,NULL,0),(924,13,260,6,32,NULL,0),(925,13,227,36,32,NULL,0),(926,13,263,28,32,NULL,0),(927,13,243,63,32,NULL,0),(928,13,231,32,32,NULL,0),(929,13,212,75,32,NULL,0),(930,13,244,22,32,NULL,0),(931,14,238,0,32,NULL,0),(932,14,250,1,32,NULL,0),(933,14,249,2,32,NULL,0),(934,14,239,3,32,NULL,0),(935,14,209,4,32,NULL,0),(936,14,197,5,32,NULL,0),(937,14,225,6,32,NULL,0),(938,14,216,7,32,NULL,0),(939,14,200,8,32,NULL,0),(940,14,247,9,32,NULL,0),(941,14,213,10,32,NULL,0),(942,14,262,11,32,NULL,0),(943,14,196,12,32,NULL,0),(944,14,189,13,32,NULL,0),(945,14,198,14,32,NULL,0),(946,14,178,15,32,NULL,0),(947,14,185,16,32,NULL,0),(948,14,232,17,32,NULL,0),(949,14,184,18,32,NULL,0),(950,14,195,19,32,NULL,0),(951,14,214,20,32,NULL,0),(952,14,179,21,32,NULL,0),(953,14,246,22,32,NULL,0),(954,14,251,23,32,NULL,0),(955,14,173,24,32,NULL,0),(956,14,191,25,32,NULL,0),(957,14,248,26,32,NULL,0),(958,14,255,27,32,NULL,0),(959,14,254,28,32,NULL,0),(960,14,207,29,32,NULL,0),(961,14,180,30,32,NULL,0),(962,14,202,31,32,NULL,0),(963,14,233,32,32,NULL,0),(964,14,252,33,32,NULL,0),(965,14,235,34,32,NULL,0),(966,14,240,35,32,NULL,0),(967,14,253,36,32,NULL,0),(968,14,242,37,32,NULL,0),(969,14,244,38,32,NULL,0),(970,14,228,39,32,NULL,0),(971,14,203,40,32,NULL,0),(972,14,224,41,32,NULL,0),(973,14,259,42,32,NULL,0),(974,14,188,43,32,NULL,0),(975,14,234,44,32,NULL,0),(976,14,193,45,32,NULL,0),(977,14,218,46,32,NULL,0),(978,14,221,47,32,NULL,0),(979,14,211,48,32,NULL,0),(980,14,223,49,32,NULL,0),(981,14,206,50,32,NULL,0),(982,14,210,51,32,NULL,0),(983,14,260,52,32,NULL,0),(984,14,205,53,32,NULL,0),(985,14,243,54,32,NULL,0),(986,14,227,55,32,NULL,0),(987,14,230,56,32,NULL,0),(988,14,215,57,32,NULL,0),(989,14,192,58,32,NULL,0),(990,14,261,59,32,NULL,0),(991,14,177,60,32,NULL,0),(992,14,256,61,32,NULL,0),(993,14,212,62,32,NULL,0),(994,14,231,63,32,NULL,0),(995,14,181,64,32,NULL,0),(996,14,208,65,32,NULL,0),(997,14,194,66,32,NULL,0),(998,14,226,67,32,NULL,0),(999,14,236,68,32,NULL,0),(1000,14,183,69,32,NULL,0),(1001,14,217,70,32,NULL,0),(1002,14,199,71,32,NULL,0),(1003,14,220,72,32,NULL,0),(1004,14,171,73,32,NULL,0),(1005,14,219,74,32,NULL,0),(1006,14,201,75,32,NULL,0),(1007,14,222,76,32,NULL,0),(1008,14,204,77,32,NULL,0),(1009,14,174,78,32,NULL,0),(1010,14,176,79,32,NULL,0),(1011,14,258,80,32,NULL,0),(1012,14,257,81,32,NULL,0),(1013,14,241,82,32,NULL,0),(1014,14,245,83,32,NULL,0),(1015,14,190,84,32,NULL,0),(1016,14,182,85,32,NULL,0),(1017,14,175,86,32,NULL,0),(1018,14,263,87,32,NULL,0),(1019,14,229,88,32,NULL,0),(1020,14,187,89,32,NULL,0),(1021,14,237,90,32,NULL,0),(1022,14,186,91,32,NULL,0),(1023,14,172,92,32,NULL,0);
/*!40000 ALTER TABLE `game_cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_users`
--

DROP TABLE IF EXISTS `game_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_users` (
  `game_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `game_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `game_user_lives` int(11) NOT NULL DEFAULT '3',
  `game_user_order` int(11) DEFAULT NULL,
  `game_user_vote` int(11) NOT NULL DEFAULT '0' COMMENT '-1 = vote for attack, 1 = vote for defend',
  PRIMARY KEY (`game_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=174 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_users`
--

LOCK TABLES `game_users` WRITE;
/*!40000 ALTER TABLE `game_users` DISABLE KEYS */;
INSERT INTO `game_users` VALUES (66,13,1,0,1,0),(173,14,1,3,1,0);
/*!40000 ALTER TABLE `game_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `games`
--

DROP TABLE IF EXISTS `games`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `games` (
  `game_id` int(11) NOT NULL AUTO_INCREMENT,
  `game_name` varchar(100) DEFAULT 'stupid-duel',
  `owner_user_id` int(11) NOT NULL,
  `deck_id` int(11) NOT NULL,
  `game_password` varchar(45) DEFAULT NULL,
  `game_turn` int(11) NOT NULL DEFAULT '0',
  `game_start_time` varchar(45) DEFAULT NULL,
  `game_defender_id` int(11) DEFAULT NULL,
  `game_attack_scenario` longtext,
  `game_defend_scenario` longtext,
  `game_over` tinyint(1) NOT NULL DEFAULT '0',
  `game_coin_toss` tinyint(1) NOT NULL DEFAULT '0',
  `game_round_recap` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`game_id`),
  KEY `owner_user_id_idx` (`owner_user_id`),
  KEY `deck_id_idx` (`deck_id`),
  CONSTRAINT `deck_id` FOREIGN KEY (`deck_id`) REFERENCES `decks` (`deck_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `owner_user_id` FOREIGN KEY (`owner_user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `games`
--

LOCK TABLES `games` WRITE;
/*!40000 ALTER TABLE `games` DISABLE KEYS */;
INSERT INTO `games` VALUES (13,'USCB CSci',1,32,'the_manana',0,NULL,NULL,NULL,NULL,0,0,NULL),(14,'Test Game',1,32,'password',0,NULL,NULL,NULL,NULL,0,0,NULL);
/*!40000 ALTER TABLE `games` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gods`
--

DROP TABLE IF EXISTS `gods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gods` (
  `god_id` int(11) NOT NULL AUTO_INCREMENT,
  `god_name` varchar(100) NOT NULL,
  PRIMARY KEY (`god_id`),
  UNIQUE KEY `god_name_UNIQUE` (`god_name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gods`
--

LOCK TABLES `gods` WRITE;
/*!40000 ALTER TABLE `gods` DISABLE KEYS */;
INSERT INTO `gods` VALUES (1,'Bruce Lee'),(4,'Christopher Lee'),(3,'Darth Vader'),(5,'Neo'),(2,'Shia LaBeouf');
/*!40000 ALTER TABLE `gods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(100) NOT NULL,
  PRIMARY KEY (`item_id`),
  UNIQUE KEY `item_name_UNIQUE` (`item_name`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (7,'alarm clock'),(9,'bat'),(27,'battery'),(12,'bazooka'),(25,'belt'),(31,'book'),(17,'bottle'),(19,'brick'),(30,'cord'),(32,'dildo'),(2,'duck'),(11,'fish'),(13,'grenade'),(8,'gun'),(26,'hammer'),(22,'knife'),(14,'monitor'),(28,'nailgun'),(15,'paper clip'),(24,'pillow'),(29,'plastic bag'),(36,'sandwich'),(20,'shoe'),(21,'toothbrush'),(18,'trophy'),(23,'water'),(16,'wine glass');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mods`
--

DROP TABLE IF EXISTS `mods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mods` (
  `mod_id` int(11) NOT NULL AUTO_INCREMENT,
  `mod_name` varchar(100) NOT NULL,
  PRIMARY KEY (`mod_id`),
  UNIQUE KEY `modifier_name_UNIQUE` (`mod_name`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mods`
--

LOCK TABLES `mods` WRITE;
/*!40000 ALTER TABLE `mods` DISABLE KEYS */;
INSERT INTO `mods` VALUES (5,'acidic/poisonous'),(12,'diseased'),(18,'elastic'),(2,'electric'),(3,'explosive'),(4,'flaming'),(6,'freezing'),(16,'indestructable'),(17,'invisible'),(11,'magic'),(10,'massive'),(14,'radioactive'),(13,'spikey/sharp'),(15,'toxic'),(7,'x10'),(8,'x100'),(9,'x1000');
/*!40000 ALTER TABLE `mods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) NOT NULL,
  `user_password` varchar(100) DEFAULT NULL,
  `user_permission_level` int(11) NOT NULL DEFAULT '1',
  `user_login_session` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name_UNIQUE` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (0,'admin','139f16ccb476e014c66c44aa521526cbcc8aff1c',4,'c1dfd96eea8cc2b62785275bca38ac261256e278'),(1,'sweylo','ef1e22e009d84f9329161b2652cde45647da265f',3,'9e6a55b6b4563e652a23be9d623ca5055c356940'),(2,'guest',NULL,0,NULL),(9,'test','51abb9636078defbf888d8457a7c76f85c8f114c',1,'0716d9708d321ffb6a00818614779e779925365c'),(10,'junk','ad6fc8d7178edb54a5fad25ffaa7327b02718fea',1,'77de68daecd823babbb58edb1c8e14d7106e83bb'),(11,'jive','a07ad501f149a28ce2e0bc95261a0472d4be68c2',1,'bd307a3ec329e10a2cff8fb87480823da114f8f4'),(12,'groove','e138a69746e79ec1da2ff5fb4297f6373fb432fa',1,'77de68daecd823babbb58edb1c8e14d7106e83bb'),(16,'Ghost9687','8cc6bfc8cee37fc22c48ad6c85896251277700f9',1,'0ade7c2cf97f75d009975f4d720d1fa6c19f4897'),(17,'Pharoah00','00434bdef1961d85ec786bde83278dd235f35acc',1,'c1dfd96eea8cc2b62785275bca38ac261256e278');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wilds`
--

DROP TABLE IF EXISTS `wilds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wilds` (
  `wild_id` int(11) NOT NULL AUTO_INCREMENT,
  `wild_name` varchar(100) NOT NULL,
  PRIMARY KEY (`wild_id`),
  UNIQUE KEY `wildcard_name_UNIQUE` (`wild_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wilds`
--

LOCK TABLES `wilds` WRITE;
/*!40000 ALTER TABLE `wilds` DISABLE KEYS */;
INSERT INTO `wilds` VALUES (2,'MacGyver'),(1,'Magic Genie');
/*!40000 ALTER TABLE `wilds` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-13 11:00:38
