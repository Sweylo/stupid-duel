-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: sweylo.net    Database: stupid-duel
-- ------------------------------------------------------
-- Server version	5.6.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `card_gods`
--

DROP TABLE IF EXISTS `card_gods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_gods` (
  `card_god_id` int(11) NOT NULL AUTO_INCREMENT,
  `god_id` int(11) NOT NULL,
  `god_card_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`card_god_id`),
  UNIQUE KEY `god_card_id_UNIQUE` (`god_card_id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_gods`
--

LOCK TABLES `card_gods` WRITE;
/*!40000 ALTER TABLE `card_gods` DISABLE KEYS */;
INSERT INTO `card_gods` VALUES (12,1,257),(13,2,258),(14,3,259),(15,4,260),(16,5,261);
/*!40000 ALTER TABLE `card_gods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card_items`
--

DROP TABLE IF EXISTS `card_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_items` (
  `card_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `item_card_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`card_item_id`),
  UNIQUE KEY `item_card_id_UNIQUE` (`item_card_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1041 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_items`
--

LOCK TABLES `card_items` WRITE;
/*!40000 ALTER TABLE `card_items` DISABLE KEYS */;
INSERT INTO `card_items` VALUES (159,2,171),(160,2,172),(161,7,173),(162,7,174),(163,8,175),(164,8,176),(165,9,177),(166,9,178),(167,11,179),(168,11,180),(169,12,181),(170,12,182),(171,13,183),(172,13,184),(173,14,185),(174,14,186),(175,15,187),(176,15,188),(177,16,189),(178,16,190),(179,17,191),(180,17,192),(181,18,193),(182,18,194),(183,19,195),(184,19,196),(185,20,197),(186,20,198),(187,21,199),(188,21,200),(189,22,201),(190,22,202),(191,23,203),(192,23,204),(193,24,205),(194,24,206),(195,25,207),(196,25,208),(197,26,209),(198,26,210),(199,27,211),(200,27,212),(201,28,213),(202,28,214),(203,29,215),(204,29,216),(205,30,217),(206,30,218),(207,31,219),(208,31,220),(209,32,221),(210,32,222),(885,2,264),(886,2,265),(887,7,266),(888,7,267),(889,8,268),(890,8,269),(891,9,270),(892,9,271),(893,11,272),(894,11,273),(895,12,274),(896,12,275),(897,13,276),(898,13,277),(899,14,278),(900,14,279),(901,15,280),(902,15,281),(903,16,282),(904,16,283),(905,17,284),(906,17,285),(907,18,286),(908,18,287),(909,19,288),(910,19,289),(911,20,290),(912,20,291),(913,21,292),(914,21,293),(915,22,294),(916,22,295),(917,23,296),(918,23,297),(919,24,298),(920,24,299),(921,25,300),(922,25,301),(923,26,302),(924,26,303),(925,27,304),(926,27,305),(927,28,306),(928,28,307),(929,29,308),(930,29,309),(931,30,310),(932,30,311),(933,31,312),(934,31,313),(935,32,314),(936,32,315);
/*!40000 ALTER TABLE `card_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card_mods`
--

DROP TABLE IF EXISTS `card_mods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_mods` (
  `card_mod_id` int(11) NOT NULL AUTO_INCREMENT,
  `mod_id` int(11) NOT NULL,
  `mod_card_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`card_mod_id`),
  UNIQUE KEY `mod_card_id_UNIQUE` (`mod_card_id`)
) ENGINE=InnoDB AUTO_INCREMENT=513 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_mods`
--

LOCK TABLES `card_mods` WRITE;
/*!40000 ALTER TABLE `card_mods` DISABLE KEYS */;
INSERT INTO `card_mods` VALUES (3,2,223),(4,2,224),(5,3,225),(6,3,226),(7,4,227),(8,4,228),(9,5,229),(10,5,230),(11,6,231),(12,6,232),(13,7,233),(14,7,234),(15,8,235),(16,8,236),(17,9,237),(18,9,238),(19,10,239),(20,10,240),(21,11,241),(22,11,242),(23,12,243),(24,12,244),(25,13,245),(26,13,246),(27,14,247),(28,14,248),(29,15,249),(30,15,250),(31,16,251),(32,16,252),(33,17,253),(34,17,254),(35,18,255),(36,18,256),(411,2,316),(412,2,317),(413,3,318),(414,3,319),(415,4,320),(416,4,321),(417,5,322),(418,5,323),(419,6,324),(420,6,325),(421,7,326),(422,7,327),(423,8,328),(424,8,329),(425,9,330),(426,9,331),(427,10,332),(428,10,333),(429,11,334),(430,11,335),(431,12,336),(432,12,337),(433,13,338),(434,13,339),(435,14,340),(436,14,341),(437,15,342),(438,15,343),(439,16,344),(440,16,345),(441,17,346),(442,17,347),(443,18,348),(444,18,349);
/*!40000 ALTER TABLE `card_mods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card_wilds`
--

DROP TABLE IF EXISTS `card_wilds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_wilds` (
  `card_wild_id` int(11) NOT NULL AUTO_INCREMENT,
  `wild_id` int(11) NOT NULL,
  `wild_card_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`card_wild_id`),
  UNIQUE KEY `wild_card_id_UNIQUE` (`wild_card_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_wilds`
--

LOCK TABLES `card_wilds` WRITE;
/*!40000 ALTER TABLE `card_wilds` DISABLE KEYS */;
INSERT INTO `card_wilds` VALUES (1,1,262),(2,2,263);
/*!40000 ALTER TABLE `card_wilds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cards`
--

DROP TABLE IF EXISTS `cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cards` (
  `card_id` int(11) NOT NULL AUTO_INCREMENT,
  `card_deck_id` int(11) NOT NULL,
  PRIMARY KEY (`card_id`),
  KEY `card_deck_id_idx` (`card_deck_id`),
  CONSTRAINT `card_deck_id` FOREIGN KEY (`card_deck_id`) REFERENCES `decks` (`deck_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=350 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cards`
--

LOCK TABLES `cards` WRITE;
/*!40000 ALTER TABLE `cards` DISABLE KEYS */;
INSERT INTO `cards` VALUES (171,32),(172,32),(173,32),(174,32),(175,32),(176,32),(177,32),(178,32),(179,32),(180,32),(181,32),(182,32),(183,32),(184,32),(185,32),(186,32),(187,32),(188,32),(189,32),(190,32),(191,32),(192,32),(193,32),(194,32),(195,32),(196,32),(197,32),(198,32),(199,32),(200,32),(201,32),(202,32),(203,32),(204,32),(205,32),(206,32),(207,32),(208,32),(209,32),(210,32),(211,32),(212,32),(213,32),(214,32),(215,32),(216,32),(217,32),(218,32),(219,32),(220,32),(221,32),(222,32),(223,32),(224,32),(225,32),(226,32),(227,32),(228,32),(229,32),(230,32),(231,32),(232,32),(233,32),(234,32),(235,32),(236,32),(237,32),(238,32),(239,32),(240,32),(241,32),(242,32),(243,32),(244,32),(245,32),(246,32),(247,32),(248,32),(249,32),(250,32),(251,32),(252,32),(253,32),(254,32),(255,32),(256,32),(257,32),(258,32),(259,32),(260,32),(261,32),(262,32),(263,32),(264,48),(265,48),(266,48),(267,48),(268,48),(269,48),(270,48),(271,48),(272,48),(273,48),(274,48),(275,48),(276,48),(277,48),(278,48),(279,48),(280,48),(281,48),(282,48),(283,48),(284,48),(285,48),(286,48),(287,48),(288,48),(289,48),(290,48),(291,48),(292,48),(293,48),(294,48),(295,48),(296,48),(297,48),(298,48),(299,48),(300,48),(301,48),(302,48),(303,48),(304,48),(305,48),(306,48),(307,48),(308,48),(309,48),(310,48),(311,48),(312,48),(313,48),(314,48),(315,48),(316,48),(317,48),(318,48),(319,48),(320,48),(321,48),(322,48),(323,48),(324,48),(325,48),(326,48),(327,48),(328,48),(329,48),(330,48),(331,48),(332,48),(333,48),(334,48),(335,48),(336,48),(337,48),(338,48),(339,48),(340,48),(341,48),(342,48),(343,48),(344,48),(345,48),(346,48),(347,48),(348,48),(349,48);
/*!40000 ALTER TABLE `cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `decks`
--

DROP TABLE IF EXISTS `decks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `decks` (
  `deck_id` int(11) NOT NULL AUTO_INCREMENT,
  `deck_name` varchar(100) NOT NULL DEFAULT 'new deck',
  `deck_creator_user_id` int(11) NOT NULL,
  PRIMARY KEY (`deck_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `decks`
--

LOCK TABLES `decks` WRITE;
/*!40000 ALTER TABLE `decks` DISABLE KEYS */;
INSERT INTO `decks` VALUES (32,'standard',1),(48,'standard_nogods_nowilds',1);
/*!40000 ALTER TABLE `decks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_cards`
--

DROP TABLE IF EXISTS `game_cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_cards` (
  `game_card_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'This is a table to store the shuffled order of cards in the game.',
  `card_id` int(11) NOT NULL,
  `card_order_index` int(11) NOT NULL,
  `deck_id` int(11) NOT NULL,
  `user_hand_id` int(11) DEFAULT NULL,
  `game_id` int(11) NOT NULL,
  `game_card_field_status` int(11) NOT NULL DEFAULT '0' COMMENT 'null if not on field, 1 if is on attack side, 2 if is on defend side, -1 for a being in discard',
  PRIMARY KEY (`game_card_id`)
) ENGINE=InnoDB AUTO_INCREMENT=931 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_cards`
--

LOCK TABLES `game_cards` WRITE;
/*!40000 ALTER TABLE `game_cards` DISABLE KEYS */;
INSERT INTO `game_cards` VALUES (838,223,0,32,NULL,13,0),(839,179,1,32,NULL,13,0),(840,235,2,32,NULL,13,0),(841,253,3,32,NULL,13,0),(842,190,4,32,NULL,13,0),(843,187,5,32,NULL,13,0),(844,183,6,32,NULL,13,0),(845,201,7,32,NULL,13,0),(846,213,8,32,NULL,13,0),(847,202,9,32,NULL,13,0),(848,180,10,32,NULL,13,0),(849,184,11,32,NULL,13,0),(850,239,12,32,NULL,13,0),(851,238,13,32,NULL,13,0),(852,171,14,32,NULL,13,0),(853,173,15,32,NULL,13,0),(854,178,16,32,NULL,13,0),(855,191,17,32,NULL,13,0),(856,220,18,32,NULL,13,0),(857,177,19,32,NULL,13,0),(858,224,20,32,NULL,13,0),(859,241,21,32,NULL,13,0),(860,245,22,32,NULL,13,0),(861,209,23,32,NULL,13,0),(862,229,24,32,NULL,13,0),(863,188,25,32,NULL,13,0),(864,195,26,32,NULL,13,0),(865,193,27,32,NULL,13,0),(866,246,28,32,NULL,13,0),(867,196,29,32,NULL,13,0),(868,175,30,32,NULL,13,0),(869,247,31,32,NULL,13,0),(870,257,32,32,NULL,13,0),(871,250,33,32,NULL,13,0),(872,234,34,32,NULL,13,0),(873,215,35,32,NULL,13,0),(874,225,36,32,NULL,13,0),(875,249,37,32,NULL,13,0),(876,214,38,32,NULL,13,0),(877,172,39,32,NULL,13,0),(878,233,40,32,NULL,13,0),(879,203,41,32,NULL,13,0),(880,219,42,32,NULL,13,0),(881,228,43,32,NULL,13,0),(882,200,44,32,NULL,13,0),(883,216,45,32,NULL,13,0),(884,242,46,32,NULL,13,0),(885,230,47,32,NULL,13,0),(886,252,48,32,NULL,13,0),(887,185,49,32,NULL,13,0),(888,176,50,32,NULL,13,0),(889,197,51,32,NULL,13,0),(890,210,52,32,NULL,13,0),(891,182,53,32,NULL,13,0),(892,258,54,32,NULL,13,0),(893,218,55,32,NULL,13,0),(894,174,56,32,NULL,13,0),(895,254,57,32,NULL,13,0),(896,232,58,32,NULL,13,0),(897,262,59,32,NULL,13,0),(898,236,60,32,NULL,13,0),(899,206,61,32,NULL,13,0),(900,194,62,32,NULL,13,0),(901,189,63,32,NULL,13,0),(902,222,64,32,NULL,13,0),(903,208,65,32,NULL,13,0),(904,207,66,32,NULL,13,0),(905,255,67,32,NULL,13,0),(906,205,68,32,NULL,13,0),(907,192,69,32,NULL,13,0),(908,226,70,32,NULL,13,0),(909,198,71,32,NULL,13,0),(910,181,72,32,NULL,13,0),(911,261,73,32,NULL,13,0),(912,186,74,32,NULL,13,0),(913,256,75,32,NULL,13,0),(914,248,76,32,NULL,13,0),(915,221,77,32,NULL,13,0),(916,211,78,32,NULL,13,0),(917,251,79,32,NULL,13,0),(918,204,80,32,NULL,13,0),(919,237,81,32,NULL,13,0),(920,240,82,32,NULL,13,0),(921,217,83,32,NULL,13,0),(922,199,84,32,NULL,13,0),(923,259,85,32,NULL,13,0),(924,260,86,32,NULL,13,0),(925,227,87,32,NULL,13,0),(926,263,88,32,NULL,13,0),(927,243,89,32,NULL,13,0),(928,231,90,32,NULL,13,0),(929,212,91,32,NULL,13,0),(930,244,92,32,NULL,13,0);
/*!40000 ALTER TABLE `game_cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_users`
--

DROP TABLE IF EXISTS `game_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_users` (
  `game_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `game_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `game_user_lives` int(11) NOT NULL DEFAULT '3',
  `game_user_order` int(11) DEFAULT NULL,
  `game_user_vote` int(11) NOT NULL DEFAULT '0' COMMENT '-1 = vote for attack, 1 = vote for defend',
  PRIMARY KEY (`game_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_users`
--

LOCK TABLES `game_users` WRITE;
/*!40000 ALTER TABLE `game_users` DISABLE KEYS */;
INSERT INTO `game_users` VALUES (66,13,1,3,1,0);
/*!40000 ALTER TABLE `game_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `games`
--

DROP TABLE IF EXISTS `games`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `games` (
  `game_id` int(11) NOT NULL AUTO_INCREMENT,
  `game_name` varchar(100) DEFAULT 'stupid-duel',
  `owner_user_id` int(11) NOT NULL,
  `deck_id` int(11) NOT NULL,
  `game_password` varchar(45) DEFAULT NULL,
  `game_turn` int(11) NOT NULL DEFAULT '0',
  `game_start_time` varchar(45) DEFAULT NULL,
  `game_defender_id` int(11) DEFAULT NULL,
  `game_attack_scenario` longtext,
  `game_defend_scenario` longtext,
  `game_over` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`game_id`),
  KEY `owner_user_id_idx` (`owner_user_id`),
  KEY `deck_id_idx` (`deck_id`),
  CONSTRAINT `deck_id` FOREIGN KEY (`deck_id`) REFERENCES `decks` (`deck_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `owner_user_id` FOREIGN KEY (`owner_user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `games`
--

LOCK TABLES `games` WRITE;
/*!40000 ALTER TABLE `games` DISABLE KEYS */;
INSERT INTO `games` VALUES (13,'USCB CSci',1,32,'the_manana',0,NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `games` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gods`
--

DROP TABLE IF EXISTS `gods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gods` (
  `god_id` int(11) NOT NULL AUTO_INCREMENT,
  `god_name` varchar(100) NOT NULL,
  PRIMARY KEY (`god_id`),
  UNIQUE KEY `god_name_UNIQUE` (`god_name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gods`
--

LOCK TABLES `gods` WRITE;
/*!40000 ALTER TABLE `gods` DISABLE KEYS */;
INSERT INTO `gods` VALUES (1,'Bruce Lee'),(4,'Christopher Lee'),(3,'Darth Vader'),(5,'Neo'),(2,'Shia LaBeouf');
/*!40000 ALTER TABLE `gods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(100) NOT NULL,
  PRIMARY KEY (`item_id`),
  UNIQUE KEY `item_name_UNIQUE` (`item_name`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (7,'alarm clock'),(9,'bat'),(27,'battery'),(12,'bazooka'),(25,'belt'),(31,'book'),(17,'bottle'),(19,'brick'),(30,'cord'),(32,'dildo'),(2,'duck'),(11,'fish'),(13,'grenade'),(8,'gun'),(26,'hammer'),(22,'knife'),(14,'monitor'),(28,'nailgun'),(15,'paper clip'),(24,'pillow'),(29,'plastic bag'),(20,'shoe'),(21,'toothbrush'),(18,'trophy'),(23,'water'),(16,'wine glass');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mods`
--

DROP TABLE IF EXISTS `mods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mods` (
  `mod_id` int(11) NOT NULL AUTO_INCREMENT,
  `mod_name` varchar(100) NOT NULL,
  PRIMARY KEY (`mod_id`),
  UNIQUE KEY `modifier_name_UNIQUE` (`mod_name`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mods`
--

LOCK TABLES `mods` WRITE;
/*!40000 ALTER TABLE `mods` DISABLE KEYS */;
INSERT INTO `mods` VALUES (5,'acidic/poisonous'),(12,'diseased'),(18,'elastic'),(2,'electric'),(3,'explosive'),(4,'flaming'),(6,'freezing'),(16,'indestructable'),(17,'invisible'),(11,'magic'),(10,'massive'),(14,'radioactive'),(13,'spikey/sharp'),(15,'toxic'),(7,'x10'),(8,'x100'),(9,'x1000');
/*!40000 ALTER TABLE `mods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `user_permission_level` int(11) NOT NULL DEFAULT '1',
  `user_login_session` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name_UNIQUE` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (0,'admin','139f16ccb476e014c66c44aa521526cbcc8aff1c',4,'902ba3cda1883801594b6e1b452790cc53948fda'),(1,'sweylo','ef1e22e009d84f9329161b2652cde45647da265f',2,'da4b9237bacccdf19c0760cab7aec4a8359010b0'),(9,'test','51abb9636078defbf888d8457a7c76f85c8f114c',1,'fe5dbbcea5ce7e2988b8c69bcfdfde8904aabc1f'),(10,'junk','ad6fc8d7178edb54a5fad25ffaa7327b02718fea',1,'b1d5781111d84f7b3fe45a0852e59758cd7a87e5'),(11,'jive','a07ad501f149a28ce2e0bc95261a0472d4be68c2',1,'c1dfd96eea8cc2b62785275bca38ac261256e278'),(12,'groove','e138a69746e79ec1da2ff5fb4297f6373fb432fa',1,'ac3478d69a3c81fa62e60f5c3696165a4e5e6ac4');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wilds`
--

DROP TABLE IF EXISTS `wilds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wilds` (
  `wild_id` int(11) NOT NULL AUTO_INCREMENT,
  `wild_name` varchar(100) NOT NULL,
  PRIMARY KEY (`wild_id`),
  UNIQUE KEY `wildcard_name_UNIQUE` (`wild_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wilds`
--

LOCK TABLES `wilds` WRITE;
/*!40000 ALTER TABLE `wilds` DISABLE KEYS */;
INSERT INTO `wilds` VALUES (2,'MacGyver'),(1,'Magic Genie');
/*!40000 ALTER TABLE `wilds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'stupid-duel'
--

--
-- Dumping routines for database 'stupid-duel'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-19  1:27:03
