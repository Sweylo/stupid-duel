<?php

$user_check = true;
include('global.php');

$deck = $db -> select_one_from('decks', 'deck_id', $_GET['deck_id']);
$cards = $db -> select_many_from('cards', 'card_deck_id', $deck['deck_id']);

//shuffle($cards);

?>

<body>

	<div class="content">

		<div class="content-header">

			<h1>Edit Deck - <?php echo $deck['deck_name']; ?></h1>

			<?php
			if (
				$deck['deck_creator_user_id'] == $user['user_id']
				||
				$user['user_permission_level'] > 1
			) {
			?>
			<a class="button delete right"
				href="delete_deck.php?id=<?php echo $deck[deck_id]; ?>">
				Delete
			</a>
			<?php } ?>

			<div class="clear"></div>
		</div>

		<ul><?php

		foreach ($cards as $card) {
			$this_card = $db -> get_card($card['card_id']);
			echo "<li>$this_card[card_name], $this_card[card_type]</li>";
		}

		?></ul>

	</div>

<?php include('footer.php'); ?>
