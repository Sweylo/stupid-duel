<?php

$user_check = false;
include('global.php');

if (
	!$GLOBALS['config']['enable_guest']
	&&
	isset($_COOKIE['user']) 
	&& 
	isset($_COOKIE['login_session'])
) {
    header('Location: index.php');
}

?>

<!DOCTYPE html>

<head>
    <title>Stupid-Duel - Login</title>
	<link rel="stylesheet" type="text/css" href="css/reset.css" />
	<link rel="stylesheet" type="text/css" href="css/fonts.css" />
	<link rel="stylesheet" type="text/css" href="css/main.css" />
	<link rel="stylesheet" type="text/css" href="css/login.css" />
</head>

<body id="login"><form method="post" action="authenticate_user.php">

	<div class="content-header">
	    <h1>Stupid-Duel</h1>
		<div class="clear"></div>
	</div>

    <div><input name="username" type="text" placeholder="Username"></div>
    <div><input name="password" type="password" placeholder="Password"></div>
    <div><input type="submit" value="Login"></div>
	<div id="create"><a href="create_user.php">Create account</a></div>

</form></body>
